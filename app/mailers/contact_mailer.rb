class ContactMailer < ApplicationMailer
  def contact_form(params)
    @params = params
    mail(to: 'contact@neighbourfood.mx', subject: "Contacto de #{params[:name]}")
  end

  def location_form(params)
    @params = params
    mail(to: 'contact@neighbourfood.mx', subject: "Solicito Nfood en #{params[:search]}")
  end
end
