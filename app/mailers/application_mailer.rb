class ApplicationMailer < ActionMailer::Base
  default from: "NeighbourFood <#{ENV['NF_MAIL_USERNAME']}>"

  layout 'mailer'
end
