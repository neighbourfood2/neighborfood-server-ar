class GeneralUserMailer < ApplicationMailer
  layout false, :only => ['order_confirmed', 'order_delivered']

  def order_delivered(user, order)
    @user = user
    @order = order
    mail(to: user.email, subject: "Tu orden #{order.menu.name} ha llegado")
  end

  def order_confirmed(user, order)
    @user = user
    @order = order
    mail(to: user.email, subject: "Tu orden  de #{order.menu.name} ya se esta cocinando")
  end

  def no_orders(user)
    @user = user
    mail(to: user.email, subject: "NFood en tu colonia próximamente")
  end
end
