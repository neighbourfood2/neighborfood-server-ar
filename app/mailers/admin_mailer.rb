class AdminMailer < ApplicationMailer

  def order_confirmed(user, order)
    @user = user
    @order = order
    mail(to: 'pedidos@neighbourfood.mx', subject: "Nuevo pedido de #{user.name}")
  end

  def starting_application
    mail(to: 'saul.ortigoza@neighbourfood.mx', subject: 'NeighbourFood Web App has Started')
  end
end
