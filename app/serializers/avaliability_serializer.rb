# ## Schema Information
#
# Table name: `avaliabilities`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `integer`          | `not null, primary key`
# **`available_time_start`**  | `time`             |
# **`available_time_end`**    | `time`             |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
#

class AvaliabilitySerializer < ActiveModel::Serializer
  attributes :id
end
