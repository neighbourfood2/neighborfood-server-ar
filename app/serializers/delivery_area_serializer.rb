# ## Schema Information
#
# Table name: `delivery_areas`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`latitude`**      | `decimal(10, 6)`   |
# **`radius`**        | `integer`          |
# **`household_id`**  | `integer`          |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`longitude`**     | `decimal(10, 6)`   |
# **`address`**       | `text`             |
# **`notes`**         | `text`             |
# **`is_archived`**   | `boolean`          |
# **`name`**          | `string`           |
#

class DeliveryAreaSerializer < ActiveModel::Serializer
  attributes :id
end
