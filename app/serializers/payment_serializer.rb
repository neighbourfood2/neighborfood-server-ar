# == Schema Information
#
# Table name: payments
#
#  id                   :integer          not null, primary key
#  date                 :datetime
#  transaction_id       :string
#  order_id             :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  type                 :string
#  total                :decimal(10, 2)
#  description          :string
#  paypal_redirect_urls :text
#

class PaymentSerializer < ActiveModel::Serializer
  attributes :id
end
