# ## Schema Information
#
# Table name: `dishes`
#
# ### Columns
#
# Name                      | Type               | Attributes
# ------------------------- | ------------------ | ---------------------------
# **`id`**                  | `integer`          | `not null, primary key`
# **`name`**                | `string`           |
# **`description`**         | `text`             |
# **`price`**               | `decimal(16, 2)`   |
# **`calories_level`**      | `integer`          |
# **`fat_level`**           | `integer`          |
# **`spice_level`**         | `integer`          |
# **`household_id`**        | `integer`          |
# **`created_at`**          | `datetime`         | `not null`
# **`updated_at`**          | `datetime`         | `not null`
# **`image_file_name`**     | `string`           |
# **`image_content_type`**  | `string`           |
# **`image_file_size`**     | `integer`          |
# **`image_updated_at`**    | `datetime`         |
# **`is_archived`**         | `boolean`          | `default(FALSE), not null`
#

class DishSerializer < ActiveModel::Serializer
  attributes :id
end
