# ## Schema Information
#
# Table name: `tmp_locations`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `integer`          | `not null, primary key`
# **`name`**        | `string`           |
# **`address`**     | `string`           |
# **`latitude`**    | `decimal(10, 6)`   |
# **`longitude`**   | `decimal(10, 6)`   |
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

class TmpLocationSerializer < ActiveModel::Serializer
  attributes :id
end
