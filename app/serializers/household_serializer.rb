# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`id`**                      | `integer`          | `not null, primary key`
# **`email`**                   | `string`           | `default(""), not null`
# **`encrypted_password`**      | `string`           | `default(""), not null`
# **`reset_password_token`**    | `string`           |
# **`reset_password_sent_at`**  | `datetime`         |
# **`remember_created_at`**     | `datetime`         |
# **`sign_in_count`**           | `integer`          | `default(0), not null`
# **`current_sign_in_at`**      | `datetime`         |
# **`last_sign_in_at`**         | `datetime`         |
# **`current_sign_in_ip`**      | `string`           |
# **`last_sign_in_ip`**         | `string`           |
# **`created_at`**              | `datetime`         | `not null`
# **`updated_at`**              | `datetime`         | `not null`
# **`type`**                    | `string`           |
# **`avatar_file_name`**        | `string`           |
# **`avatar_content_type`**     | `string`           |
# **`avatar_file_size`**        | `integer`          |
# **`avatar_updated_at`**       | `datetime`         |
# **`first_name`**              | `string`           |
# **`last_name`**               | `string`           |
# **`phone`**                   | `string`           |
# **`latitude`**                | `decimal(10, 6)`   |
# **`longitude`**               | `decimal(10, 6)`   |
# **`tmp_location_id`**         | `integer`          |
#

class HouseholdSerializer < ActiveModel::Serializer
  attributes :id
end
