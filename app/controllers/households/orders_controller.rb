class Households::OrdersController < ApplicationController
  before_action :require_household!
  before_action :set_order, only: [:show]
  before_action :set_own_order, only: [:edit, :update, :destroy]

  # GET /households/:household_id/orders
  # GET /households/:household_id/orders.json
  def index
    @orders = Order.households(current_household, params).decorate
  end

  # GET /households/:household_id/orders/1
  # GET /households/:household_id/orders/1.json
  def show
  end

  # GET /households/:household_id/orders/new
  def new
    @order = household.orders.build
  end

  # GET /households/:household_id/orders/1/edit
  def edit
  end

  # POST /households/:household_id/orders
  # POST /households/:household_id/orders.json
  def create
    @order = household.orders.build(order_params)

    respond_to do |format|
      if @order.save
        format.html { redirect_to [current_user, @order], notice: 'Order was successfully created.' }
        format.json { render json: @order, status: :created }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /households/:household_id/orders/1
  # PATCH/PUT /households/:household_id/orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to [current_user, @order], notice: 'Order was successfully updated.' }
        format.json { render json: @order, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /households/:household_id/orders/1
  # DELETE /households/:household_id/orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to household_orders_url(current_user), notice: 'Order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  #### Sub elements

  # POST /households/:household_id/orders/:order_id/deliver
  def deliver
    @order = current_household.orders.find(params[:order_id])
    @order.deliver
    if @order.save
      redirect_to [current_user, @order], notice: 'Order was successfully delivered.'
    else
      redirect_to [current_user, @order], notice: 'Order delivery was '\
      "unsuccessful: #{@order.errors.full_messages}"
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = household.orders.find(params[:id]).decorate
  end

  def set_own_order
    @order = current_household.orders.find(params[:id])
  end

  def household
    Household.find(params[:household_id])
  end

  # A list of the param names that can be used for filtering the orders list
  def filtering_params(params)
    params.slice(:state, :created_before, :created_after)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:page, :latitude, :longitude, :delivery_eta,
                                  :comment_food, :comment_order, :state, :user_id, :dish_id)
  end
end
