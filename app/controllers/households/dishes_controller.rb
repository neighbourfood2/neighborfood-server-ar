class Households::DishesController < ApplicationController
  before_action :require_household!, except: [:show, :index]
  before_action :set_dish, only: [:show]
  before_action :set_own_dish, only: [:edit, :update, :destroy]

  # GET /households/:household_id/dishes
  # GET /households/:household_id/dishes.json
  def index
    @dishes = Dish.for_household(household)
  end

  # GET /households/:household_id/dishes/:id
  # GET /households/:household_id/dishes/:id.json
  def show
  end

  # GET /households/:household_id/dishes/new
  def new
    @dish = current_household.dishes.build
  end

  # GET /households/:household_id/dishes/1/edit
  def edit
  end

  # POST /households/:household_id/dishes
  # POST /households/:household_id/dishes.json
  def create
    @dish = current_household.dishes.build(dish_params)

    respond_to do |format|
      if @dish.save
        format.html { redirect_to [current_user, @dish], notice: 'Dish was successfully created.' }
        format.json { render json: @dish, status: :created }
      else
        format.html { render :new }
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /households/:household_id/dishes/1
  # PATCH/PUT /households/:household_id/dishes/1.json
  def update
    respond_to do |format|
      if @dish.update(dish_params)
        format.html { redirect_to [current_user, @dish], notice: 'Dish was successfully updated.' }
        format.json { render json: @dish, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @dish.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /households/:household_id/dishes/1
  # DELETE /households/:household_id/dishes/1.json
  def destroy
    @dish.destroy
    respond_to do |format|
      format.html { redirect_to household_dishes_url(current_user), notice: 'Dish was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_dish
    @dish = household.dishes.find(params[:id])
  end

  def set_own_dish
    @dish = current_household.dishes.find(params[:id])
  end

  def household
    Household.find(params[:household_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def dish_params
    params.require(:dish).permit(:name, :description, :price, :calories_level, :fat_level, :spice_level, :household_id, :image)
  end
end
