class Households::MenusController < ApplicationController
  before_action :require_household!
  before_action :set_menu, only: [:show, :edit, :update, :destroy]

  # GET /menus
  # GET /menus.json
  def index
    @menus = Menu.households(household, params[:page]).decorate
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    @orders = Order.in_menu(@menu, params[:page]).decorate
  end

  # GET /menus/new
  def new
    @menu = household.menus.new.decorate
    @dishes = Dish.for_household(household)
    @delivery_areas = household.delivery_areas
  end

  # GET /menus/1/edit
  def edit
    @dishes = Dish.for_household(household)
    @delivery_areas = household.delivery_areas
  end

  # POST /menus
  # POST /menus.json
  def create
    @menu = household.menus.build(menu_params).decorate
    @dishes = Dish.for_household(household)
    @delivery_areas = household.delivery_areas

    if @menu.save
      redirect_to [current_user, @menu], notice: 'Menu was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    @dishes = Dish.for_household(household)
    respond_to do |format|
      if @menu.update(menu_params.except(:main_dish_id, :dish_ids))
        format.html { redirect_to [current_user, @menu], notice: 'Menu was successfully updated.' }
        format.json { render json: @menu, status: :ok }
      else
        format.html { render :edit }
        format.json { render json: @menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to household_menus_url(current_user), notice: 'Menu was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_menu
    @menu = Menu.find(params[:id]).decorate
  end

  def household
    Household.find(params[:household_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def menu_params
    params.require(:menu).permit(:description,
                                 :delivery_eta,
                                 :household_id,
                                 :delivery_area_id,
                                 :raw_price,
                                 :quantity,
                                 :main_dish_id,
                                 :block_sale,
                                 dish_ids: [])
  end
end
