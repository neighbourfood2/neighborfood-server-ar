class Households::DeliveryAreasController < ApplicationController
  before_action :require_household!, except: [:show, :index]

  def index
    @delivery_areas = current_household.delivery_areas
  end

  def new
    @delivery_area = current_household.delivery_areas.build
  end

  def edit
    set_delivery_area
  end

  def show
    set_delivery_area
  end

  def create
    @delivery_area = current_household.delivery_areas.build(delivery_area_params)
    if @delivery_area.save
      redirect_to household_delivery_areas_path(current_user), notice: 'Delivery area was successfully created.'
    else
      render :new
    end
  end

  def update
    set_delivery_area
    if @delivery_area.update delivery_area_params
      redirect_to [current_user, @delivery_area], notice: 'Delivery area was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    set_delivery_area
    @delivery_area.destroy
    redirect_to household_delivery_areas_path(current_user), notice: 'Dish was successfully destroyed.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_delivery_area
    @delivery_area = DeliveryArea.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def delivery_area_params
    params.require(:delivery_area).permit(
      :name,
      :latitude,
      :longitude,
      :address,
      :radius,
      :delivery_area_id,
      :household_id,
      :tmp_location_id)
  end
end
