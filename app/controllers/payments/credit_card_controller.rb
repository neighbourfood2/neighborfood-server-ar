class Payments::CreditCardController < ApplicationController
  def new
    @order = Order.find(payment_params[:order_id])
  end

  def create
    @payment = Payment::CreditCard.new order_id: payment_params[:order_id]
    if @payment.process payment_params
      redirect_to general_user_order_path(payment_params[:user_id], @payment.order),
                  notice: 'Order was successfully created.'
    else
      render :new
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def payment_params
    params.require(:payment).permit(:order_id,
                                    :user_id,
                                    :payment_type,
                                    :credit_card_type,
                                    :type,
                                    :number,
                                    :expire_month,
                                    :expire_year,
                                    :cvv2,
                                    :first_name,
                                    :last_name)
  end
end
