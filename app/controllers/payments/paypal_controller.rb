class Payments::PaypalController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:paypal_confirm_webhook]

  def create
    @payment = Payment::Paypal.new order_id: paypal_params[:order_id]
    redirect_to @payment.approval_url if @payment.process
  end

  def paypal_confirm
    @payment = Payment::Paypal.where(transaction_id: params[:paymentId]).take

    unless @payment
      return redirect_to root_path, notice: "No se ha encontrado el pago, por favor pongase en contacto con nosotros" unless current_general_user
      redirect_to general_user_orders_path(current_general_user),
                  notice: "No se ha encontrado el pago, por favor pongase en contacto con nosotros"
      logger.info "@payment no se encontro. #{params.to_s}"
      return
    end

    if @payment.execute params
      return redirect_to root_path, notice: "Su pago se ha realizado con éxito" unless current_general_user
      redirect_to general_user_order_path(current_general_user, @payment.payable),
                  notice: 'Order was successfully created.'
    else
      return redirect_to root_path, notice: "Ocurrió un error en la transacción, por favor pongase en contacto con nosotros" unless current_general_user
      redirect_to general_user_order_path(current_general_user, @payment.payable),
                  notice: "Ocurrió un error en la transacción... (#{@payment.errors.full_messages})"
      logger.info @payment.errors.full_messages
    end
  end

  # Execute payment when webhook is recieved from paypal
  # TODO: implement test case
  def paypal_confirm_webhook
    # logger.info '############################################'
    # logger.info JSON.parse params
    render nothing: true
  end

  private

  def paypal_params
    params.require(:payment).permit(:order_id,
                                    :payment_type,
                                    :user_id)
  end
end
