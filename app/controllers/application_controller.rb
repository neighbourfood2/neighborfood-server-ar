class ApplicationController < ActionController::Base
  include UsersHelper
  #before_action :alpha_notice
  #before_action :prelaunch_redirect

  protect_from_forgery with: :exception

  helper_method :account_url, :current_admin, :current_household,
                :require_admin!, :load_default_user

  def prelaunch_redirect
    redirect_to "http://prelaunch.neighbourfood.mx/" unless params[:no]
  end

  def alpha_notice
    flash.discard[:alert] = %Q{Estás usando una versión alfa y es posible que 
    encuentres errores. Cualquier comentario, duda o sugerencia 
    comunícate al (33) 1399-0678 o a 
    <a href="mailto:contacto@neighbourfood.mx">contacto@neighbourfood.mx</a>. 
    ¡Gracias por ser un usuario alfa!}
  end

end
