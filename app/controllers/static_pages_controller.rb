class StaticPagesController < ApplicationController
  def about_us
  end

  def company
  end

  def help
  end

  def legal
  end

  def block_sale
  end

  def contact_cookers
  end

  def contact_service
  end

  def contact_general
  end
end
