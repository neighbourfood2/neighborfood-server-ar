class LandingPageController < ApplicationController
  def index
    @menus_sample = Menu.samples.decorate
  end

  def contact
    subscription = Subscription.new
    subscription.add_contact subscription_params
    ContactMailer.contact_form(subscription_params).deliver_now
    redirect_to :root, flash: { notice: '¡Gracias!' }
  end

  def subscribe
    subscription = Subscription.new
    if subscription.create(subscription_params)
      redirect_to :root, flash: { notice: '¡Gracias!' }
    else
      redirect_to :root, flash: { error: "Error! #{subscription.error}" }
    end
  end

  def suscribe_location
    SearchEntry.log_search params
    ContactMailer.location_form(params).deliver_now
    redirect_to :root, flash: { notice: '¡Gracias! Pronto estaremos contigo' }
  end

  private

  def subscription_params
    params.require(:subscription).permit(:contact_type,
                                         :email,
                                         :name,
                                         :address,
                                         :city,
                                         :state,
                                         :phone,
                                         :message)
  end
end
