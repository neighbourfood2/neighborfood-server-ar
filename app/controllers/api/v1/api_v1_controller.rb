class Api::V1::ApiController < ApplicationController
  protect_from_forgery with: :null_session,
                       if: proc { |c| c.request.format == 'application/json' }

  respond_to :json

  def failure(status, params = {})
    render status: status,
           json: {
             success: false,
             info: params[:info] || 'Api::V1::#failure',
             data: params[:data] || {},
             message: params[:message] || '',
             error_code: params[:error_code] || 1
           }
  end

  def success(data, params = {})
    render status: 200,
           json: {
             success: true,
             info: params[:info] || '',
             data: data,
             message: params[:message] || '',
             error_code: params[:error_code] || 0
           }
  end
end
