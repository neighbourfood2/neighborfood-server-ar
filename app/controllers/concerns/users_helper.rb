module UsersHelper
  extend ActiveSupport::Concern

  def account_url
    return new_user_session_url unless user_signed_in?
    case current_user.class.name
    when 'GeneralUser'
      root_path
    when 'Household'
      household_orders_path(current_user)
    else
      root_url
    end if user_signed_in?
  end

  def load_default_user
    @current_user ||= GeneralUser.new unless current_user
  end

  protected

  def current_admin
    @current_admin ||= current_user if
    user_signed_in? && current_user.class.name == 'Admin'
  end

  def current_household
    @current_household ||= current_user if
    user_signed_in? && current_user.class.name == 'Household'
  end

  def current_general_user
    @current_general_user ||= current_user if
    user_signed_in? && current_user.class.name == 'GeneralUser'
  end

  def admin_logged_in?
    @admin_logged_in ||= user_signed_in? && current_admin
  end

  def household_logged_in?
    @household_logged_in ||= user_signed_in? && current_household
  end

  def general_user_logged_in?
    @general_user_logged_in ||= user_signed_in? && current_general_user
  end

  def require_admin!(*_args)
    require_user_type(:admin)
  end

  alias_method(:authenticate_admin!, :require_admin!)

  def require_household!(*_args)
    require_user_type(:household)
  end

  alias_method(:authenticate_household!, :require_household!)

  def require_general_user!(*_args)
    require_user_type(:general_user)
  end

  alias_method(:authenticate_general_user!, :require_general_user!)

  def require_user_type(user_type)
    return if user_type == :admin && admin_logged_in?
    return if user_type == :household && household_logged_in?
    return if user_type == :general_user && general_user_logged_in?

    redirect_to root_path, status: 301,
                           notice: "You must be logged in a#{'n' if
                           user_type == :admin} #{user_type} to access"\
                           ' this content'
    false
  end
end
