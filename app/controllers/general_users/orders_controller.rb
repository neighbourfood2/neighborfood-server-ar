class GeneralUsers::OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update]

  def index
    @orders = Order.users(current_user, params[:page]).decorate
  end

  def show
    @menu = @order.menu
  end

  def create
    tmp_params = order_params.except(:payment_type)
    tmp_params[:comment_order] += get_address
    
    @order = Order.build_user_order(current_user, tmp_params)
    @menu = Menu.where(id: order_params[:menu_id]).includes(:household).first.decorate
    if @order.save
      op = OrderProcessor.new(@order, order_params)
      
      return redirect_to block_sale_path if op.block_sale?

      paypal_paths = { return: payments_paypal_confirm_path, 
                       cancel: payments_paypal_confirm_path}
      if op.do_paypal_payment paypal_paths
        return redirect_to op.payment.paypal_redirect_urls[:approval_url][:href]
      end
      if op.credit_card?
        return redirect_to new_payments_credit_card_path(payment: { order_id: @order.id })
      end
      render :new
    else
      render 'menus/show'
    end
  end

  def update
    logger.info order_update_params
    @menu = @order.menu
    if @order.update order_update_params
      redirect_to [current_user, @order], notice: '¡Orden calificada con éxito!'
    else
      redirect_to [current_user, @order], notice: @order.errors
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_order
    @order = current_user.orders.find params[:id]
  end

  def get_address
    "Dirección: 
    Calle: #{params[:addr_street]}, 
    no. Exterior: #{params[:addr_ext_no]}, 
    no. interior: #{params[:addr_int_no]}, 
    Entre calles: #{params[:addr_bt_streets]}. \n"
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def order_params
    params.require(:order).permit(:latitude, :longitude, :delivery_eta,
                                  :comment_food, :comment_order, :state,
                                  :user_id, :menu_id, :household_id,
                                  :payment_type, :t_and_c_acepted, :quantity)
  end

  def order_update_params
    params.require(:order).permit(:comment_rate, :comment_order, :comment_food)
  end
end
