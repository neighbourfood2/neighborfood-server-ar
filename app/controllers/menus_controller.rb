# ## Schema Information
#
# Table name: `menus`
#
# ### Columns
#
# Name                    | Type               | Attributes
# ----------------------- | ------------------ | ---------------------------
# **`id`**                | `integer`          | `not null, primary key`
# **`created_at`**        | `datetime`         | `not null`
# **`updated_at`**        | `datetime`         | `not null`
# **`date`**              | `date`             |
# **`household_id`**      | `integer`          |
# **`raw_price`**         | `decimal(16, 2)`   |
# **`quantity`**          | `integer`          |
# **`total_left`**        | `integer`          |
# **`delivery_eta`**      | `datetime`         |
# **`description`**       | `text`             |
# **`is_archived`**       | `boolean`          |
# **`charge`**            | `decimal(16, 2)`   |
# **`delivery_area_id`**  | `integer`          |
# **`block_sale`**        | `boolean`          |
#

class MenusController < ApplicationController
  def index
    SearchEntry.log_search params
    #menus = Menu.location_filter(params[:latitude], params[:longitude]) + Menu.upcomming(1)
    @menus2 = Menu.upcomming_by_day.inject({}) do |hash, (k, arr)| 
      hash[k] = MenuDecorator.decorate_collection(arr)
      hash
    end
    #@menus = MenuDecorator.decorate_collection(menus)
  end

  def show
    @menu = Menu.where(id: params[:id]).includes(:household).first.decorate
    if current_general_user
      @order = current_general_user.orders
               .build menu: @menu,
                      household: @menu.household
    end
  end
end
