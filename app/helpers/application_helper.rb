module ApplicationHelper
  def site_name
    'NeighbourFood'
  end

  def site_url
    if Rails.env.production?
      'http://neighbourfood.mx'
    else
      'http://localhost:3000/'
    end
  end

  def meta_author
    'NeighborFood'
  end

  def meta_description
    'NeighbourFood es una plataforma que conecta a los mejores cocineros del 
    vecindario con los comenzales más exigentes que buscan comida casera y 
    nutritiva.'
  end

  def meta_keywords
    %w{neighbourfood food comida casera}
  end

  def full_title(page_title='')
    if page_title.empty?
      site_name
    else
      "#{page_title} | #{site_name}"
    end
  end

  def bootstrap_class_for(flash_type)
    { success: 'alert-success', error: 'alert-danger', alert: 'alert-warning', notice: 'alert-info' }[flash_type.to_sym] || flash_type.to_s
  end

  def bourbon_class_for(flash_type)
    { success: 'flash-success', error: 'flash-error', alert: 'flash-alert', notice: 'flash-notice' }[flash_type.to_sym] || flash_type.to_s
  end
end
