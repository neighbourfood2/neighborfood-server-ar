# ## Schema Information
#
# Table name: `search_entries`
#
# ### Columns
#
# Name               | Type               | Attributes
# ------------------ | ------------------ | ---------------------------
# **`id`**           | `integer`          | `not null, primary key`
# **`text_entry`**   | `text`             |
# **`field1`**       | `string`           |
# **`field2`**       | `string`           |
# **`field3`**       | `string`           |
# **`search_type`**  | `string`           |
# **`created_at`**   | `datetime`         | `not null`
# **`updated_at`**   | `datetime`         | `not null`
#

class SearchEntry < ActiveRecord::Base
  def self.log_search(params)
    log = new text_entry: params[:search], field1: params[:latitude],
              field2: params[:longitude], search_type: :location
    logger.tagged('MENU_SEARCH') do
      logger.info "{address: \"#{log.text_entry}\",\
     latitude: #{log.field1}, longitude: #{log.field2}"
    end
    log.save
  end
end
