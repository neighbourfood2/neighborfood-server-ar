# ## Schema Information
#
# Table name: `payments`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `integer`          | `not null, primary key`
# **`type`**                  | `string`           |
# **`sale`**                  | `decimal(10, 2)`   |
# **`description`**           | `string`           |
# **`paypal_redirect_urls`**  | `text`             |
# **`payed_at`**              | `datetime`         |
# **`transaction_id`**        | `string`           |
# **`payable_id`**            | `integer`          |
# **`payable_type`**          | `string`           |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
# **`charge`**                | `decimal(16, 2)`   |
#

class Payment::Base < ActiveRecord::Base
  self.table_name = 'payments'

  belongs_to :payable, polymorphic: true

  validates :sale, :charge, :description, presence: true

  def process(_params)
    quantity = payable.quantity || 1
    self.sale = payable.menu.raw_price * quantity
    self.charge = payable.menu.charge * quantity
    self.description = "#{payable.quantity} ordenes de #{payable.menu.name} por #{payable.menu.public_price * quantity}"
  end

  def total
    quantity = payable.quantity || 1
    payable.menu.public_price * quantity
  end
end
