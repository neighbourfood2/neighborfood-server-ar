# ## Schema Information
#
# Table name: `payments`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `integer`          | `not null, primary key`
# **`type`**                  | `string`           |
# **`sale`**                  | `decimal(10, 2)`   |
# **`description`**           | `string`           |
# **`paypal_redirect_urls`**  | `text`             |
# **`payed_at`**              | `datetime`         |
# **`transaction_id`**        | `string`           |
# **`payable_id`**            | `integer`          |
# **`payable_type`**          | `string`           |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
# **`charge`**                | `decimal(16, 2)`   |
#

require 'paypal-sdk-rest'

class Payment::Paypal < Payment::Base
  serialize :paypal_redirect_urls, Hash

  attr_accessor :return_url, :cancel_url

  def process(params = {})
    super(params)
    return false unless payable.ready_to_pay? self
    return false unless paypal
    return true if do_payment
    false
  end

  def execute(params)
    payment = PayPal::SDK::REST::Payment.find(transaction_id)

    return false unless payable.ready_to_pay? self
    if payment.execute(payer_id: params[:PayerID])
      self.payed_at = DateTime.now.in_time_zone
      payable.payed
      self.save(validate: false)
      return true
    else
      errors.add :base, payment.error.to_s
    end
    false
  end

  private

  def paypal
    paypal_urls_valid?
    return false unless valid?

    redirect_urls = {
      return_url: "http://#{ENV['NF_DOMAIN_NAME']}#{return_url}",
      cancel_url: "http://#{ENV['NF_DOMAIN_NAME']}#{cancel_url}"
    }

    @payment = PayPal::SDK::REST::Payment.new(intent: 'sale',
                                              payer: build_paypal_payer,
                                              transactions: transactions_array,
                                              redirect_urls: redirect_urls)
  end

  def do_payment
    if @payment.create
      self.transaction_id = @payment.id
      set_paypal_redirect_urls
      save!
      return true
    else
      errors.add :base, "payment error: #{@payment.error}"
    end
    false
  end

  def set_paypal_redirect_urls
    a_hash = @payment.links.inject({}) do |ha, a|
      ha.store(a.rel.to_sym, href: a.href, method: a.method); ha
    end
    self.paypal_redirect_urls = a_hash
  end

  def paypal_urls_valid?
    errors.add :return_url, 'return url not set' if return_url.nil?
    errors.add :cancel_url, 'cancel url not set' if cancel_url.nil?
  end

  def build_paypal_payer
    {
      payment_method: 'paypal'
    }
  end

  # Returns an array with one transaction composed of
  # self total and with self description.
  def transactions_array
    [{
      amount: {
        total: format_total(total),
        currency: 'MXN'
      },
      description: description
    }]
  end

  # Format total to two decimal places
  def format_total(number)
    format('%.2f', number)
  end
end
