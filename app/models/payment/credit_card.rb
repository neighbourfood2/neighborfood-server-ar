# ## Schema Information
#
# Table name: `payments`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `integer`          | `not null, primary key`
# **`type`**                  | `string`           |
# **`sale`**                  | `decimal(10, 2)`   |
# **`description`**           | `string`           |
# **`paypal_redirect_urls`**  | `text`             |
# **`payed_at`**              | `datetime`         |
# **`transaction_id`**        | `string`           |
# **`payable_id`**            | `integer`          |
# **`payable_type`**          | `string`           |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
# **`charge`**                | `decimal(16, 2)`   |
#

require 'paypal-sdk-rest'

class Payment::CreditCard < Payment::Base
  attr_accessor :order_id, :token

  def process(params)
    super(params)
    return false unless credit_card params
    return type if do_payment
    false
  end

  private

  def credit_card(params)
    validate_c_card_params params
    return false unless valid?

    @payment = PayPal::SDK::REST::Payment.new(intent: 'sale',
                                              payer: build_c_card_payer(params),
                                              transactions: transactions_array)
  end

  def do_payment
    # begin
    if @payment.create
      self.transaction_id = @payment.id
      self.date = DateTime.now.in_time_zone
      order.payed
      save!
      return true
    else
      errors.add :base, "payment error: #{@payment.error}"
    end
    # rescue Exception => e
    #  errors.add :base, "payment error: #{e}"
    #  logger.error e
    # end
    false
  end

  def build_c_card_payer(params)
    {
      payment_method: type,
      funding_instruments: [{
        credit_card: {
          type: params[:credit_card_type], # e.g. visa
          number: params[:number], # e.g. 4417119669820331
          expire_month: params[:expire_month], # e.g. 11
          expire_year: params[:expire_year], # e.g. 2018
          cvv2: params[:cvv2], # e.g. 325
          first_name: params[:first_name], # e.g. Joe
          last_name: params[:last_name], # e.g. Shopper
        }
      }]
    }
  end

  def transactions_array
    # Returns an array with one transaction composed of
    # self total and with self description.
    [{
      amount: {
        total: format_total(total),
        currency: 'MXN'
      },
      description: description
    }]
  end

  def validate_c_card_params(params)
    errors.add :base, 'missing type' unless params.key?(:credit_card_type)
    errors.add :base, 'missing number' unless params.key?(:number)
    errors.add :base, 'missing expire_month' unless params.key?(:expire_month)
    errors.add :base, 'missing expire_year' unless params.key?(:expire_year)
    errors.add :base, 'missing cvv2' unless params.key?(:cvv2)
    errors.add :base, 'missing first_name' unless params.key?(:first_name)
    errors.add :base, 'missing last_name' unless params.key?(:last_name)
  end

  def format_total(number)
    format('%.2f', number)
    # '%.2f' % number
  end
end
