class Charges

  CHARGE_PER = 0.0
  CHARGE_FIX = 15.0

  def initialize menu
    @menu = menu
    #@menu.charge = charge unless @menu.charge
  end

  # Aditional amount charged by NFood
  def charge
    (@menu.raw_price * CHARGE_PER) + CHARGE_FIX
  end

  def set_charge
    @menu.charge = charge
  end

  # Amount shown to general public. At wich they buy.
  def public_price
    @menu.raw_price + @menu.charge
  end

  # Total amount of menus sold
  def number_sold
    (@menu.quantity - @menu.total_left)
  end

  # Amount to be payed to the cooker
  def total_cooker_sale
    @menu.number_sold * @menu.raw_price
  end

  # Total public sale
  def total_sale
    @menu.number_sold * public_price
  end

  # Total amount of charged. Amount won by NFood
  def total_charges
    @menu.number_sold * @menu.charge
  end

end