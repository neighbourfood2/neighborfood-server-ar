# ## Schema Information
#
# Table name: `menu_dishes`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`menu_id`**       | `integer`          |
# **`dish_id`**       | `integer`          |
# **`quantity`**      | `integer`          |
# **`total_left`**    | `integer`          |
# **`is_main_dish`**  | `boolean`          |
#

class MenuDish < ActiveRecord::Base
  belongs_to :menu
  belongs_to :dish
end
