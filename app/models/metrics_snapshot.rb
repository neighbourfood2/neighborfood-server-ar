# ## Schema Information
#
# Table name: `metrics_snapshots`
#
# ### Columns
#
# Name                  | Type               | Attributes
# --------------------- | ------------------ | ---------------------------
# **`id`**              | `integer`          | `not null, primary key`
# **`total_sale`**      | `integer`          |
# **`total_charges`**   | `integer`          |
# **`sign_ups_count`**  | `integer`          |
# **`sign_ins_count`**  | `integer`          |
# **`total_users`**     | `integer`          |
# **`date`**            | `datetime`         |
# **`created_at`**      | `datetime`         | `not null`
# **`updated_at`**      | `datetime`         | `not null`
#

class MetricsSnapshot < ActiveRecord::Base

  def compute_for date
    self.date = date
    load_total_sale
    load_total_charges
    load_sign_ups_count
    load_sign_ins_count
    load_total_users
    load_total_sign_ins_count
    load_orders_count
    load_menus_sold_count
  end

  validates  :total_sale, :total_charges, :sign_ups_count, :sign_ins_count, 
            :total_users, :date, presence: true

  def load_total_sale
    self.total_sale = Payment::Base.where("DATE(created_at) = ? AND payed_at IS NOT NULL", date.to_date).sum(:sale).to_i
  end

  def load_total_charges
    self.total_charges = Payment::Base.where("DATE(created_at) = ? AND payed_at IS NOT NULL", date.to_date).sum(:charge).to_i
  end

  def load_sign_ups_count
    self.sign_ups_count = GeneralUser.where("DATE(created_at) = ?", date.to_date).count
  end

  def load_sign_ins_count
    self.sign_ins_count = GeneralUser.where("DATE(current_sign_in_at) = ?", date.to_date).count
  end

  def load_total_users
    self.total_users = GeneralUser.count
  end

  def load_total_sign_ins_count
    self.total_sign_ins_count = GeneralUser.sum(:sign_in_count)
  end

  def load_orders_count
    self.orders_count = Order.where("DATE(created_at) = ? AND state != ?", date.to_date, 'placed').count
  end

  def load_menus_sold_count
    self.menus_sold_count = Order.where("DATE(created_at) = ? AND state != ?", date.to_date, 'placed').sum(:quantity)
  end

  def save_if_none
    ms = MetricsSnapshot.where("DATE(date) = ?", date.to_date).take
    save if ms.nil?
  end

end
