# ## Schema Information
#
# Table name: `delivery_areas`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`latitude`**      | `decimal(10, 6)`   |
# **`radius`**        | `integer`          |
# **`household_id`**  | `integer`          |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`longitude`**     | `decimal(10, 6)`   |
# **`address`**       | `text`             |
# **`notes`**         | `text`             |
# **`is_archived`**   | `boolean`          |
# **`name`**          | `string`           |
#

class DeliveryArea < ActiveRecord::Base
  # Macros
  default_value_for :is_archived, false

  default_scope { where.not is_archived: true } 

  # Relationships

  belongs_to :household
  has_many :menus

  # Validations

  validates :name, :address, :latitude, :longitude, :radius, presence: true

  # Methods
 def destroy
    self.is_archived = true
    save(validate: false)
  end
end
