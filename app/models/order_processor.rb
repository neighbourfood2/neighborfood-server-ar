class OrderProcessor

  attr_accessor :payment

  def initialize order, params
    @order = order
    @params = params
    @type = params[:payment_type].to_sym
  end

  def block_sale?
    return false unless @order.menu.block_sale
    true
  end

  def credit_card?
    return true if @type == :credit_card
    false
  end

  def paypal?
    return true if @type == :paypal
    false
  end

  def do_paypal_payment paths
    return false unless paypal?
    @payment = Payment::Paypal.new payable: @order,
                                   return_url: paths[:return],
                                   cancel_url: paths[:cancel]
    return true if @payment.process @params
    false
  end

end