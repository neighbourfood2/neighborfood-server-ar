# ## Schema Information
#
# Table name: `dishes`
#
# ### Columns
#
# Name                      | Type               | Attributes
# ------------------------- | ------------------ | ---------------------------
# **`id`**                  | `integer`          | `not null, primary key`
# **`name`**                | `string`           |
# **`description`**         | `text`             |
# **`price`**               | `decimal(16, 2)`   |
# **`calories_level`**      | `integer`          |
# **`fat_level`**           | `integer`          |
# **`spice_level`**         | `integer`          |
# **`household_id`**        | `integer`          |
# **`created_at`**          | `datetime`         | `not null`
# **`updated_at`**          | `datetime`         | `not null`
# **`image_file_name`**     | `string`           |
# **`image_content_type`**  | `string`           |
# **`image_file_size`**     | `integer`          |
# **`image_updated_at`**    | `datetime`         |
# **`is_archived`**         | `boolean`          | `default(FALSE), not null`
#

class Dish < ActiveRecord::Base
  # Macros

  # Relationships

  belongs_to :household
  has_many :menu_dishes
  has_many :menus, through: :menu_dishes

  # Validations

  validates :name, :description, :image, presence: true

  # Methods

  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100>' },
                            default_url: 'missing.png'
  validates_attachment_content_type :image, content_type: %r{\Aimage\/.*\Z}

  def destroy
    self.is_archived = true
    save(validate: false)
  end

  def self.for_household(household)
    household.dishes.where(is_archived: false)
  end
end
