# ## Schema Information
#
# Table name: `order_statuses`
#
# ### Columns
#
# Name                     | Type               | Attributes
# ------------------------ | ------------------ | ---------------------------
# **`id`**                 | `integer`          | `not null, primary key`
# **`name`**               | `string`           |
# **`created_at`**         | `datetime`         | `not null`
# **`updated_at`**         | `datetime`         | `not null`
# **`status_identifier`**  | `string`           |
#

class OrderStatus < ActiveRecord::Base
end
