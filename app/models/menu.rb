# ## Schema Information
#
# Table name: `menus`
#
# ### Columns
#
# Name                    | Type               | Attributes
# ----------------------- | ------------------ | ---------------------------
# **`id`**                | `integer`          | `not null, primary key`
# **`created_at`**        | `datetime`         | `not null`
# **`updated_at`**        | `datetime`         | `not null`
# **`date`**              | `date`             |
# **`household_id`**      | `integer`          |
# **`raw_price`**         | `decimal(16, 2)`   |
# **`quantity`**          | `integer`          |
# **`total_left`**        | `integer`          |
# **`delivery_eta`**      | `datetime`         |
# **`description`**       | `text`             |
# **`is_archived`**       | `boolean`          |
# **`charge`**            | `decimal(16, 2)`   |
# **`delivery_area_id`**  | `integer`          |
# **`block_sale`**        | `boolean`          |
#

class Menu < ActiveRecord::Base
  extend Forwardable
  # Macros and constants

  KM_TO_GPS_DEGREES = 0.008992805755

  before_create :set_total_left, :set_charge
  default_value_for :is_archived, false

  # Relationships

  belongs_to :household
  belongs_to :delivery_area
  has_many :orders
  has_many :menu_dishes
  has_many :dishes, through: :menu_dishes

  # Scopes

  scope :households, -> (hh, page) do
    hh.menus.includes(:menu_dishes, :delivery_area)
      .where(is_archived: false)
      .order(delivery_eta: :desc)
      .page(page).per(10)
  end

  scope :upcomming, -> (page) do
    where('delivery_eta >= ?', DateTime.now.in_time_zone)
      .order(:delivery_eta)
      .includes(:household, :menu_dishes, :delivery_area)
      .page(page)
  end

  scope :samples, -> () do
    where('delivery_eta >= ?', DateTime.now.in_time_zone)
      .includes(:menu_dishes)
      .limit(9)
  end

  # Menu services
  scope :not_archived, -> () { where(is_archived: false) }
  # filters scopes
  scope :start_time, -> (time) { where('delivery_eta >= ?', time) if time }
  scope :end_time, -> (time) { where('delivery_eta < ?', time) if time }
  scope :cooker, -> (cooker) { where(household_id: cooker) if cooker }
  scope :location_filter, -> (latitude, longitude, radius=500) do
    return if latitude.nil? || longitude.nil?
    radius = 500 if radius.nil?
    lat, lon = Menu.process_location latitude, longitude, radius

    includes(:delivery_area, :household, :dishes)
    .where(delivery_areas: { latitude: lat[:low]...lat[:high] })
    .where(delivery_areas: { longitude: lon[:low]...lon[:high] })
    .not_archived
    .order(:delivery_eta)
    .start_time(DateTime.now)
  end
  scope :ids_array, -> (ids_array) do
    where(id: ids_array) if ids_array
  end
  # filter_params = {start_time, end_time, latitude, longitude, radius, page, cooker, ids_array}
  scope :filters, -> (params) do
    where(nil)
    .includes(:household, :menu_dishes, :delivery_area)
    .not_archived
    .order(:delivery_eta)
    .ids_array(params[:ids_array])
    .cooker(params[:cooker])
    .start_time(params[:start_time])
    .end_time(params[:end_time])
    .location_filter(params[:latitude], params[:longitude], params[:radius])
    .page(params[:page])
  end

  def self.upcomming_by_day
    menus = Menu
    .includes(:delivery_area, :household, :dishes)
    .start_time(DateTime.now)
    .end_time(DateTime.now + 7.days)
    .not_archived
    .order(:delivery_eta)

    menu_hash = menus.inject({}) do |menu_hash, m|
      key = m.delivery_eta.to_date.to_s
      next menu_hash unless m.delivery_area # Filter out menus with no location defined
      if menu_hash.key?(key)
        menu_hash[key] << m
      else
        menu_hash[key] = [m]
      end
      menu_hash
    end

  end

  # Validations

  validates :delivery_eta, :raw_price, :quantity, presence: true
  validates :household, :menu_dishes, :delivery_area, presence: true

  # Methods

  after_initialize do 
     @charges = Charges.new self
  end

  def name
    main_dish.name if main_dish
  end

  def by_name
    household.name if household
  end
  alias_method :cooker_name, :by_name

  def main_dish
    main_dish = menu_dishes.find { |a| a.is_main_dish = true }
    main_dish.dish
  end

  def main_dish_id=(arg_id)
    menu_dishes.build(dish_id: arg_id, is_main_dish: true)
  end

  def side_dishes
    menu_dishes.select { |a| a.is_main_dish = false }
  end

  def order_taken amount=1
    unless has_orders? amount
      errors.add_to_base(:no_menus_left)
      return false
    end
    self.total_left -= amount
    save(validate: false)
  end

  def has_orders? amount=1
    return false if (total_left == 0) || (total_left - amount < 0)
    true
  end

  def destroy
    self.is_archived = true
    save(validate: false)
  end

  # Charges logic
  def_delegators :@charges, :public_price, :number_sold, :total_cooker_sale,
                 :total_sale, :total_charges, :set_charge
  alias_method :price, :public_price
  alias_method :total_hh_sale, :total_cooker_sale

  def self.process_location latitude, longitude, radius
    radius = radius * KM_TO_GPS_DEGREES # km to coordinates degrees
    lat = {}
    lon = {}
    latitude = latitude.to_f
    longitude = longitude.to_f
    lat[:low] = latitude - radius
    lat[:high] = latitude + radius
    lon[:low] = longitude - radius
    lon[:high] = longitude + radius
    [lat, lon]
  end

  private

  def set_total_left
    self.total_left = quantity
  end

end
