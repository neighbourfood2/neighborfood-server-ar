# ## Schema Information
class Subscription
  attr_reader :error

  def create(params)
    fields = {}
    add_to_list(subscribe_list, params[:email], fields)
  end

  def add_contact(params)
    fields = {
      NAME: params[:name] || 'null',
      CITY: params[:city] || 'null',
      ADDRESS: params[:address] || 'MX',
      STATE: params[:state] || 'null',
      PHONE: params[:phone] || 'null',
      MESSAGE: params[:message] || 'null',
      C_TYPE: params[:contact_type] || 'null'
    }
    add_to_list(general_list, params[:email], fields)
  end

  def add_user(params)
    fields = {
      NAME: params[:name] || 'null',
      CITY: params[:city] || 'null',
      ADDRESS: params[:address] || 'null',
      STATE: params[:state] || 'null',
      PHONE: params[:phone] || 'null',
      MESSAGE: params[:message] || 'null'
    }
    add_to_list(users_list, params[:email], fields)
  end

  def add_household(params)
    fields = {
      NAME: params[:name] || 'null',
      CITY: params[:city] || 'null',
      ADDRESS: params[:address] || 'null',
      STATE: params[:state] || 'null',
      PHONE: params[:phone] || 'null',
      MESSAGE: params[:message] || 'null'
    }
    add_to_list(households_list, params[:email], fields)
  end

  private

  def add_to_list(list, email, merge_fields)
    @error = ''
    begin
      Gibbon::Request.lists(list).members.create(
        body: { email_address: email,
                status: 'subscribed',
                merge_fields: merge_fields
          }
      )
      return true
    rescue Gibbon::MailChimpError => error
      Rails.logger.error "Gibbon::MailChimpError: #{$ERROR_INFO}, \
        #{$ERROR_INFO.title}, #{$ERROR_INFO.detail}, #{error.body}"
      @error = error
    end
    false
  end

  def users_list
    ENV['MAILCHIMP_SECRET_USERS_LIST_ID']
  end

  def households_list
    ENV['MAILCHIMP_SECRET_HOUSEHOLDS_LIST_ID']
  end

  def subscribe_list
    ENV['MAILCHIMP_SECRET_SUBSCRIBE_LIST_ID']
  end

  def general_list
    ENV['MAILCHIMP_SECRET_CONTACT_LIST_ID']
  end
end
