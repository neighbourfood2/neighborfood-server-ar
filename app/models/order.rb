# ## Schema Information
#
# Table name: `orders`
#
# ### Columns
#
# Name                   | Type               | Attributes
# ---------------------- | ------------------ | ---------------------------
# **`id`**               | `integer`          | `not null, primary key`
# **`latitude`**         | `decimal(10, 6)`   |
# **`longitude`**        | `decimal(10, 6)`   |
# **`delivery_eta`**     | `datetime`         |
# **`comment_food`**     | `text`             |
# **`comment_order`**    | `text`             |
# **`user_id`**          | `integer`          |
# **`menu_id`**          | `integer`          |
# **`created_at`**       | `datetime`         | `not null`
# **`updated_at`**       | `datetime`         | `not null`
# **`household_id`**     | `integer`          |
# **`general_user_id`**  | `integer`          |
# **`state`**            | `string`           |
# **`comment_rate`**     | `text`             |
# **`quantity`**         | `integer`          |
# **`address`**          | `string`           |
#

class Order < ActiveRecord::Base
  # Macros

  attr_accessor :t_and_c_acepted
  ratyrate_rateable 'overall_quality'
  after_update :rated, if: :comment_rate_changed?

  # Relationships

  belongs_to :household
  belongs_to :general_user
  belongs_to :menu
  has_one :payment, as: :payable, class_name: 'Payment::Base'

  # Scopes

  scope :state, -> (state) { where(state: state) if state }
  scope :created_before, -> (time) { where('delivery_eta < ?', time) if time }
  scope :created_after, -> (time) { where('delivery_eta >= ?', time) if time }
  scope :households, -> (hh, f_params) do
    hh.orders.includes({ menu: :menu_dishes }, :general_user)
      .state(f_params[:state])
      .created_before(f_params[:created_before])
      .created_after(f_params[:created_after])
      .order(delivery_eta: :desc)
      .page(f_params[:page])
  end
  scope :users, -> (user, page) do
    user.orders.where.not(state: 'placed').includes({ menu: :menu_dishes }, :household)
      .order(delivery_eta: :desc)
      .page(page)
  end
  scope :in_menu, -> (menu, page) do
    menu.orders.includes(:general_user)
      .where.not(state: 'placed')
      .page(page)
  end

  # Validations

  validates :household, :general_user, :menu, :quantity, presence: true
  validate :orders_available, :t_and_c_acepted?, :one_hr_before?, on: :create

  def orders_available
    errors.add :base, 'Ya no hay ordenes disponibles.' if menu.total_left <= 0
  end

  def t_and_c_acepted?
    errors.add :base, 'Debes aceptar los terminos y condiciones.' unless @t_and_c_acepted == '1'
  end

  def one_hr_before?
    errors.add :base, 'El tiempo de esta orden ha expirado' if
      (delivery_eta - 1.hour) < Time.zone.now
  end

  # Methods

  state_machine :state, initial: :placed do
    after_transition on: :deliver, do: :send_rate_mail
    after_transition on: :payed, do: [:order_taken, :send_order_confirmed_mail, :send_pedidos_mail]

    event :payed do
      transition placed: :confirmed
    end

    event :cancel do
      transition confirmed: :cancelled
    end

    event :deliver do
      transition confirmed: :delivered
    end

    event :rated do
      transition [:delivered, :cancelled] => :rated
    end

    state :placed
    state :confirmed
    state :cancelled
    state :delivered
    state :rated
  end

  def send_rate_mail
    GeneralUserMailer.delay.order_delivered(general_user, self)
  end

  def send_order_confirmed_mail
    GeneralUserMailer.delay.order_confirmed(general_user, self)
  end

  def send_pedidos_mail
    AdminMailer.delay.order_confirmed(general_user, self)
  end

  def ready_to_pay? obj=nil
    unless menu.has_orders? quantity
      obj.errors.add :base, "No hay suficientes platillos"
    end
    unless self.valid?
      self.errors.full_messages.each { |msg| obj.errors.add :base, "order: #{msg}" }
    end
    unless menu.valid?
      menu.errors.full_messages.each { |msg| obj.errors.add :base, "menu: #{msg}" }
    end
    obj.valid?
  end

  def order_taken
    menu.order_taken quantity
    save(validate: false)
  end

  def destroy
    cancel
    save
  end

  def self.build_user_order(user, params)
    user.orders.build(params)
  end
end
