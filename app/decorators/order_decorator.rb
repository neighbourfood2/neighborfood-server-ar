class OrderDecorator < Draper::Decorator
  delegate_all

  def name
    return menu.name if menu
    'Invalid'
  end

  def delivery_eta
    menu.delivery_eta.to_s(:military) if menu
  end

  def user_name
    general_user.name if general_user
  end

  delegate :name, to: :household, prefix: true
end
