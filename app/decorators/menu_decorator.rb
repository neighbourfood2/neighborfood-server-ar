class MenuDecorator < Draper::Decorator
  delegate_all

  def done_by
    "Cocinado por #{by_name}"
  end

  def pickup_place
    location = menu.delivery_area
    return "Se entrega en #{location.name}" if location
    'Indefinido'
  end

  def pickup_place_address
    location = menu.delivery_area
    return location.address if location
    'Lo sentimos, aun no hay dirección'
  end

  def pickup_eta
    "Se entrega el #{l(menu.delivery_eta, format: :menu_day)} a las #{pickup_eta_simple}"
  end

  def pickup_eta_simple
    "#{l(menu.delivery_eta, format: :am_pm_time)}".sub('AM', 'am').sub('PM', ' pm ')
  end

  def price
    "$ #{menu.price} MXN"
  end

  def raw_price
    "$ #{menu.raw_price} MXN"
  end

  def raw_price_simple
    menu.raw_price
  end

  def charge
    "$ #{menu.charge} MXN"
  end

  def total_hh_sale
    "$ #{menu.total_hh_sale} MXN"
  end

  def left
    return "Quedan #{total_left}" if total_left > 0
    'Ya no hay ordenes disponibles'
  end

  def image
    menu.main_dish.image if menu.main_dish
  end
end
