module YARD
  module Server
    module Commands
      class DisplayFileCommand < LibraryCommand
        def run
          # path = library.source_path
          filename = File.cleanpath(File.join(library.source_path, path))
          fail NotFoundError unless File.file?(filename)
          if filename =~ /\.(jpe?g|gif|png|bmp|svg)$/i
            headers['Content-Type'] = StaticFileCommand::DefaultMimeTypes[Regexp.last_match(1).downcase] ||
                                      ('image/svg+xml' if Regexp.last_match(1).downcase == 'svg') || 'text/html'
            render IO.read(filename)
          else
            file = CodeObjects::ExtraFileObject.new(filename)
            options.update(object: Registry.root, type: :layout, file: file)
            # options.update :object => Registry.root,
            #               :type => :layout,
            #               :file => file,
            #               :index => index ? true : false
            render
          end
        end
      end

      class StaticFileCommand < Base
        DefaultMimeTypes['svg'] = 'image/svg+xml'
      end
    end
  end
end

path = File.expand_path('../../yard-template/default/fulldoc/html/', File.dirname(__FILE__))
YARD::Server.register_static_path path
