namespace :app_metrics do
  task default: :day_snapshot

  desc 'Computes and save previows day metrics'
  task yesterday_snapshot: :environment do
    ms = MetricsSnapshot.new
    ms.compute_for Date.yesterday
    ms.save_if_none
  end

  desc 'Computes and save previows day metrics'
  task day_snapshot: :environment do
    ms = MetricsSnapshot.new
    ms.compute_for Date.today
    puts ms.inspect
  end

end