namespace :macros do
  task update_menus_archived: :environment do
    puts 'updating menus.is_archived field defaults'
    Menu.where(is_archived: nil).each { |a| a.is_archived = false; a.save }
    puts 'Done!'
  end

  task init_charges: :environment do
    puts 'updating menus.charge field defaults'
    Menu.where(charge: nil).each { |a| a.charge = 0.0; a.save }
    puts 'Done!'
  end
end
