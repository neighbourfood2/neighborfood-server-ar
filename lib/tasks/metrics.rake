namespace :metrics do
  task default: :all

  task :all do
    Rake::Task['metrics:flog'].invoke
    Rake::Task['metrics:best_practices'].invoke
    Rake::Task['metrics:traceroute'].invoke
    Rake::Task['metrics:rubocop'].invoke
    Rake::Task['metrics:rubycritic'].invoke
    Rake::Task['metrics:brakeman'].invoke
    Rake::Task['metrics:flay'].invoke
  end

  desc 'Run flog'
  task :flog do
    system 'flog app/'
  end

  desc 'Run ruby best practices'
  task :best_practices do
    system 'rails_best_practices .'
  end

  desc 'Run traceroute'
  task :traceroute do
    system 'rake traceroute'
  end

  desc 'Run and correct with rubocop'
  task :rubocop do
    system 'rubocop -a'
  end

  desc 'Run rubycritic'
  task :rubycritic do
    system 'rubycritic'
  end

  desc 'A static analysis security vulnerability scanner for Ruby on Rails'
  task :brakeman do
    system 'brakeman'
  end

  desc 'Flay analyzes ruby code for structural similarities'
  task :flay do
    system 'flay app/'
  end

  namespace :tests do
    task :all do
      Rake::Task['metrics:tests:flog'].invoke
      Rake::Task['metrics:tests:mutate'].invoke
      Rake::Task['metrics:tests:coverage'].invoke
    end

    task :flog do
      system 'flog spec/'
    end

    desc 'Verify tests'
    task :mutate do
      puts 'Run mutate for each model separately. mutate will not run now.'
      # system '. lib/test_mutators/run_test_mutators.bash'
    end

    desc 'Code coverage'
    task :coverage do
      system 'rake spec'
      puts 'Now check coverage/index.html'
    end
  end
end
