namespace :docs do
  task default: :all

  desc 'Run all docs'
  task :all do
    Rake::Task['docs:annotate'].invoke
    Rake::Task['docs:uml'].invoke
  end

  desc 'Annotate models using markdown'
  task annotate: :environment do
    system 'annotate  --exclude tests -f markdown'
  end

  desc 'Create UML diagrams'
  task :uml do
    system 'rake diagram:all'
    FileUtils.cp %w(doc/models_complete.svg
                    doc/models_brief.svg
                    doc/controllers_complete.svg
                    doc/controllers_brief.svg), 'docs/uml/', verbose: true
  end
end
