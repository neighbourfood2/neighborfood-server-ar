class FixOrderRelationships < ActiveRecord::Migration
  def change
    rename_column :orders, :dish_id, :menu_id
  end
end
