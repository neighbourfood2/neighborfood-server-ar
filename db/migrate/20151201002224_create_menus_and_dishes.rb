class CreateMenusAndDishes < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.timestamps null: false
      t.date :date
      t.belongs_to :household
      t.integer :quantity
      t.integer :total_left
    end

    create_table :menus_dishes, id: false do |t|
      t.belongs_to :menu, index: true
      t.belongs_to :dish, index: true
    end
  end
end
