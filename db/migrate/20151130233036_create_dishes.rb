class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.text :description
      t.decimal :price
      t.integer :calories_level
      t.integer :fat_level
      t.integer :spice_level
      t.integer :household_id

      t.timestamps null: false
    end
  end
end
