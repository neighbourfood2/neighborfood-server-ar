class AddHouseholdIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :household_id, :integer

    add_index :delivery_areas, :household_id
    add_index :orders, :household_id
    add_index :dishes, :household_id
    add_index :menus, :household_id
    add_index :orders, :user_id
    add_index :orders, :dish_id
  end
end
