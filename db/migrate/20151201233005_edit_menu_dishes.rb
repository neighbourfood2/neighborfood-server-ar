class EditMenuDishes < ActiveRecord::Migration
  def change
    remove_column :menus, :quantity, :integer
    remove_column :menus, :total_left, :integer
    add_column :menus_dishes, :quantity, :integer
    add_column :menus_dishes, :total_left, :integer

    add_attachment :dishes, :image
  end
end
