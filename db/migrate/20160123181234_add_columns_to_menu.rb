class AddColumnsToMenu < ActiveRecord::Migration
  def change
    rename_column :menus, :total, :raw_price
    add_column :menus, :charge, :decimal,{precision: 16, scale: 2}

    rename_column :payments, :total, :sale
    add_column :payments, :charge, :decimal,{precision: 16, scale: 2}
  end
end
