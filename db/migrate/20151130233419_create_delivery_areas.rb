class CreateDeliveryAreas < ActiveRecord::Migration
  def change
    create_table :delivery_areas do |t|
      t.decimal :latitude
      t.decimal :longitide
      t.integer :radio
      t.integer :household_id

      t.timestamps null: false
    end
  end
end
