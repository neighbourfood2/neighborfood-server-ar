class AddDescriptionToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :description, :text
    add_column :menus, :is_archived, :boolean
  end
end