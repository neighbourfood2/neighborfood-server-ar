class AddDaKeyToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :delivery_area_id, :integer
    add_column :delivery_areas, :name, :string
  end
end
