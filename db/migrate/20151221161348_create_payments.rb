class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.datetime :date
      t.decimal :amount, precision: 16, scale: 2
      t.string :transaction_id
      t.belongs_to :order

      t.timestamps null: false
    end
  end
end
