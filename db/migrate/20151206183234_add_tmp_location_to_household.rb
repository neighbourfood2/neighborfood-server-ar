class AddTmpLocationToHousehold < ActiveRecord::Migration
  def change
    add_column :users, :tmp_location_id, :integer

    add_index :users, :tmp_location_id
  end
end
