class CreateMetricsSnapshots < ActiveRecord::Migration
  def change
    create_table :metrics_snapshots do |t|
      t.integer :total_sale
      t.integer :total_charges
      t.integer :sign_ups_count
      t.integer :sign_ins_count
      t.integer :total_users
      t.datetime :date

      t.timestamps null: false
    end
  end
end
