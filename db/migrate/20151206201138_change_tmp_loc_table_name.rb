class ChangeTmpLocTableName < ActiveRecord::Migration
  def change
    rename_table :tpm_locations, :tmp_locations
  end
end
