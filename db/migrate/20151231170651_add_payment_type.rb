class AddPaymentType < ActiveRecord::Migration
  def change
    add_column :payments, :type, :string
    add_column :payments, :total, :decimal, {precision: 10, scale: 2}
    add_column :payments, :description, :string
    add_column :payments, :paypal_redirect_urls, :text
    remove_column :payments, :amount, :decimal
  end
end

