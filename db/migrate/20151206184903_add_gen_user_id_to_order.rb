class AddGenUserIdToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :general_user_id, :integer

    add_index :orders, :general_user_id
  end
end
