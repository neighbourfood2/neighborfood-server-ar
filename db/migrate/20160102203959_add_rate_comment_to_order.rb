class AddRateCommentToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :comment_rate, :text
    add_column :orders, :quantity, :integer
  end
end
