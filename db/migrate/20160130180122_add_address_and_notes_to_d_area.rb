class AddAddressAndNotesToDArea < ActiveRecord::Migration
  def change
    add_column :delivery_areas, :address, :text
    add_column :delivery_areas, :notes, :text
    rename_column :delivery_areas, :radio, :radius
    add_column :delivery_areas, :is_archived, :boolean
  end
end
