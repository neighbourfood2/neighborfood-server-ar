class AddArchievedToDish < ActiveRecord::Migration
  def change
    add_column :dishes, :is_archived, :boolean, default: false, null: false
  end
end
