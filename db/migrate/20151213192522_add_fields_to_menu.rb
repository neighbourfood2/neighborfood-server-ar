class AddFieldsToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :total, :decimal, precision: 16, scale: 2
    add_column :menus, :quantity, :integer
    add_column :menus, :total_left, :integer
    change_column :dishes, :price, :decimal, precision: 16, scale: 2

    remove_column :delivery_areas, :longitide, :decimal

    change_column :delivery_areas, :latitude, :decimal, precision: 10, scale: 6
    add_column :delivery_areas, :longitude, :decimal, precision: 10, scale: 6
    change_column :orders, :latitude, :decimal, precision: 10, scale: 6
    change_column :orders, :longitude, :decimal, precision: 10, scale: 6
  end
end
