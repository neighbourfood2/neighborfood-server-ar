class AddEtaToMenu < ActiveRecord::Migration
  def change
    add_column :menus, :delivery_eta, :datetime
  end
end
