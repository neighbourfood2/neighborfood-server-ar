class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.decimal :latitude
      t.decimal :longitude
      t.time :delivery_eta
      t.text :comment_food
      t.text :comment_order
      t.integer :state
      t.integer :user_id
      t.integer :dish_id

      t.timestamps null: false
    end
  end
end
