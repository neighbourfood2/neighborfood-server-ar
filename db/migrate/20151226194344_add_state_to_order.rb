class AddStateToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :state, :string
    remove_column :orders, :order_status_id, :integer
    add_column :order_statuses, :status_identifier, :string
  end
end
