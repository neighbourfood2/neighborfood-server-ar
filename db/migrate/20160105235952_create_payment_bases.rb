class CreatePaymentBases < ActiveRecord::Migration
  def change

    drop_table :payments
    
    create_table :payments do |t|
      t.string :type
      t.decimal :total, {precision: 10, scale: 2}
      t.string :description
      t.text :paypal_redirect_urls
      t.datetime :payed_at
      t.string :transaction_id
      t.references :payable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end