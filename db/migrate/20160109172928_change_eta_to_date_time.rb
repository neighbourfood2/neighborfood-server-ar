class ChangeEtaToDateTime < ActiveRecord::Migration
  def change
    change_column :orders, :delivery_eta, :datetime
  end
end
