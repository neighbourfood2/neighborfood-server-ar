class CreateTpmLocations < ActiveRecord::Migration
  def change
    create_table :tpm_locations do |t|
      t.string :name
      t.string :address
      t.decimal :latitude, precision: 10, scale: 6
      t.decimal :longitude, precision: 10, scale: 6

      t.timestamps null: false
    end
  end
end
