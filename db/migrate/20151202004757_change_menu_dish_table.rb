class ChangeMenuDishTable < ActiveRecord::Migration
  def change
    rename_table :menus_dishes, :menu_dishes
  end
end
