class ExtendMetrics < ActiveRecord::Migration
  def change
    add_column :metrics_snapshots, :total_sign_ins_count, :integer
    add_column :metrics_snapshots, :orders_count, :integer
    add_column :metrics_snapshots, :menus_sold_count, :integer
  end
end
