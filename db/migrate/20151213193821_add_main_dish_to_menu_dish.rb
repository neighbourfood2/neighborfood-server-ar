class AddMainDishToMenuDish < ActiveRecord::Migration
  def change
    add_column :menu_dishes, :is_main_dish, :boolean
  end
end
