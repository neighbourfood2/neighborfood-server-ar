class CreateSearchEntries < ActiveRecord::Migration
  def change
    create_table :search_entries do |t|
      t.text :text_entry
      t.string :field1
      t.string :field2
      t.string :field3
      t.string :search_type

      t.timestamps null: false
    end
    add_column :menus, :block_sale, :boolean
  end
end
