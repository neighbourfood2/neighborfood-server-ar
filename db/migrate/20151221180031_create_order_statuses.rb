class CreateOrderStatuses < ActiveRecord::Migration
  def change
    create_table :order_statuses do |t|
      t.string :name

      t.timestamps null: false
    end

    rename_column :orders, :state, :order_status_id
  end
end
