class AddMissingIndex < ActiveRecord::Migration
  def change
    add_index :average_caches, :rater_id
    add_index :average_caches, :rateable_id
    add_index :average_caches, :rateable_type
    add_index :overall_averages, :rateable_id
    add_index :overall_averages, :rateable_type
    add_index :payments, :order_id
  end
end
