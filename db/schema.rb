# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160607000131) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true

  create_table "average_caches", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "avg",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "average_caches", ["rateable_id"], name: "index_average_caches_on_rateable_id"
  add_index "average_caches", ["rateable_type"], name: "index_average_caches_on_rateable_type"
  add_index "average_caches", ["rater_id"], name: "index_average_caches_on_rater_id"

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "delivery_areas", force: :cascade do |t|
    t.decimal  "latitude",     precision: 10, scale: 6
    t.integer  "radius"
    t.integer  "household_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.decimal  "longitude",    precision: 10, scale: 6
    t.text     "address"
    t.text     "notes"
    t.boolean  "is_archived"
    t.string   "name"
  end

  add_index "delivery_areas", ["household_id"], name: "index_delivery_areas_on_household_id"

  create_table "dishes", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.decimal  "price",              precision: 16, scale: 2
    t.integer  "calories_level"
    t.integer  "fat_level"
    t.integer  "spice_level"
    t.integer  "household_id"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "is_archived",                                 default: false, null: false
  end

  add_index "dishes", ["household_id"], name: "index_dishes_on_household_id"

  create_table "menu_dishes", id: false, force: :cascade do |t|
    t.integer "menu_id"
    t.integer "dish_id"
    t.integer "quantity"
    t.integer "total_left"
    t.boolean "is_main_dish"
  end

  add_index "menu_dishes", ["dish_id"], name: "index_menu_dishes_on_dish_id"
  add_index "menu_dishes", ["menu_id"], name: "index_menu_dishes_on_menu_id"

  create_table "menus", force: :cascade do |t|
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.date     "date"
    t.integer  "household_id"
    t.decimal  "raw_price",        precision: 16, scale: 2
    t.integer  "quantity"
    t.integer  "total_left"
    t.datetime "delivery_eta"
    t.text     "description"
    t.boolean  "is_archived"
    t.decimal  "charge",           precision: 16, scale: 2
    t.integer  "delivery_area_id"
    t.boolean  "block_sale"
  end

  add_index "menus", ["household_id"], name: "index_menus_on_household_id"

  create_table "metrics_snapshots", force: :cascade do |t|
    t.integer  "total_sale"
    t.integer  "total_charges"
    t.integer  "sign_ups_count"
    t.integer  "sign_ins_count"
    t.integer  "total_users"
    t.datetime "date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "total_sign_ins_count"
    t.integer  "orders_count"
    t.integer  "menus_sold_count"
  end

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "status_identifier"
  end

  create_table "orders", force: :cascade do |t|
    t.decimal  "latitude",        precision: 10, scale: 6
    t.decimal  "longitude",       precision: 10, scale: 6
    t.datetime "delivery_eta"
    t.text     "comment_food"
    t.text     "comment_order"
    t.integer  "user_id"
    t.integer  "menu_id"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "household_id"
    t.integer  "general_user_id"
    t.string   "state"
    t.text     "comment_rate"
    t.integer  "quantity"
    t.string   "address"
  end

  add_index "orders", ["general_user_id"], name: "index_orders_on_general_user_id"
  add_index "orders", ["household_id"], name: "index_orders_on_household_id"
  add_index "orders", ["menu_id"], name: "index_orders_on_menu_id"
  add_index "orders", ["user_id"], name: "index_orders_on_user_id"

  create_table "overall_averages", force: :cascade do |t|
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "overall_avg",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "overall_averages", ["rateable_id"], name: "index_overall_averages_on_rateable_id"
  add_index "overall_averages", ["rateable_type"], name: "index_overall_averages_on_rateable_type"

  create_table "payments", force: :cascade do |t|
    t.string   "type"
    t.decimal  "sale",                 precision: 10, scale: 2
    t.string   "description"
    t.text     "paypal_redirect_urls"
    t.datetime "payed_at"
    t.string   "transaction_id"
    t.integer  "payable_id"
    t.string   "payable_type"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.decimal  "charge",               precision: 16, scale: 2
  end

  add_index "payments", ["payable_type", "payable_id"], name: "index_payments_on_payable_type_and_payable_id"

  create_table "rates", force: :cascade do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], name: "index_rates_on_rater_id"

  create_table "rating_caches", force: :cascade do |t|
    t.integer  "cacheable_id"
    t.string   "cacheable_type"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rating_caches", ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type"

  create_table "search_entries", force: :cascade do |t|
    t.text     "text_entry"
    t.string   "field1"
    t.string   "field2"
    t.string   "field3"
    t.string   "search_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tmp_locations", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.decimal  "latitude",   precision: 10, scale: 6
    t.decimal  "longitude",  precision: 10, scale: 6
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                                           default: "", null: false
    t.string   "encrypted_password",                              default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "type"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.decimal  "latitude",               precision: 10, scale: 6
    t.decimal  "longitude",              precision: 10, scale: 6
    t.integer  "tmp_location_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["tmp_location_id"], name: "index_users_on_tmp_location_id"

end
