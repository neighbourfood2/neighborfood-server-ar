# NeighbourFood Web Application

![Neighbourfood](images/NF_icon_logo.png)

## Synopsis

NeighbourFood es una aplicacion de intercambio de comida casera dedicada 
especialmente para nuestros amigos que trabajan en oficinas o no tienen tiempo 
de cocinar y estas preocupado spor la salud o el alto precio en restaurantes. 
Tambien esta dedicada para todas aquellas personas que gustan de cosinar en casa
 o simplemente quieren expandir las ventas de su fonda o cocina local. Esperamos
  que les sea de su agrado.

## Features List

### Usuario General

**1. Busqueda de comidas disponibles en tu ubicacion**

**2. Ordenes de comida a domicilio**

**3. Pagos electronicos (Paypal, Tarjeta de Credito)**

**4. Promociones para tus pedidos**

**5. Perfil para facilitacion de contacto y datos de ordenes**

### Cocinero

**1. Galeria de platillos**

**2. Administracion de ordenes**

**3. Creacion de comidas para su venta**

**4. Control de areas de entrega**

**5. Perfil publico del cocinero**

## Development

### Contact

* NeighbourFood website: [neighbourfood.mx](http://www.neighbourfood.mx/contact)
* Lead Architect: [saul.ortigoza@neighbourfood.mx](mailto:saul.ortigoza@neighbourfood.mx)
* NeighbourFood Contact: [contacto@neighbourfood.mx](mailto:contacto@neighbourfood.mx)

### Documentation

* YARD docs ( `yard server` )
* [Sharepoint](https://drive.google.com/drive/folders/0B47wJF8CvsssOGNwTy1IUlJsRWc)
* [Repositorio](https://bitbucket.org/neighbourfood2/neighborfood-server-ar)

### Testing

* RSpec
* [Bug incidents](https://bitbucket.org/neighbourfood2/neighborfood-server-ar/issues)

### Project Management

* [Scrumban board](https://trello.com/b/bviggIVA/neighbourfood-app)

### Servers

* Test server: [test.neighbourfood.mx/](http://test.neighbourfood.mx/)
* Production server: [neighbourfood.mx/](http://neighbourfood.mx/)

### Deployment

Deployment is done by Capistrano (dont forget ssh-add)

    cap production deploy
    cap sandbox deploy

## Changelog

See {file:CHANGELOG.md} for a list of changes.

## License

Please see the {file:LICENSE} and {file:LEGAL} documents for more information.