source 'https://rubygems.org'

gem 'rails', '4.2.5'

# Supported DBs
gem 'sqlite3'

# Authentication and authorization libraries
gem 'devise'
gem 'cancancan'
gem 'doorkeeper',             '~> 2.2.0'
gem 'omniauth',               '~> 1.2.2'
gem 'omniauth-facebook',      '~> 3.0.0'
gem 'omniauth-google-oauth2', '~> 0.2.0'

# Console
gem 'rb-readline'

# Assets and front end gems
## Compiling and serving web assets
gem 'sprockets',    '3.2.0'
## Use Uglifier as compressor for JavaScript assets
gem 'uglifier',     '>= 1.3.0'
## Use SCSS for stylesheets
gem 'sass-rails',   '~> 5.0'
## Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
## Turbolinks makes following links in your web application faster.
## Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
## Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'bootstrap-sass', '~> 3.3.1'
## Use burbon instead of bootstrap
gem 'bourbon'
gem 'neat'
gem 'bitters'
gem 'refills'
gem 'breakpoint'
## Font awesome
gem 'font-awesome-sass'
## React
gem 'react-rails', '~> 1.5.0'
## Format dates and times
## based on human-friendly examples
gem 'stamp', '~> 0.6.0'
## Static pages
gem 'high_voltage', '~> 2.2.1'
## Front end components
gem 'bower-rails', '~> 0.10.0'
gem 'metamagic'

# JSON API
gem 'jbuilder', '~> 2.0'
gem 'active_model_serializers', '0.9.2' # Model serializers for api
gem 'oj'
gem 'oj_mimic_json'

# Rating, front and backend
gem 'ratyrate'

# Backend stuff
## Env vars handling
gem 'figaro'
## HTTP requests
gem 'httparty'
## Payments
gem 'paypal-sdk-rest' # https://github.com/paypal/PayPal-Ruby-SDK
gem 'activemerchant'
## Mailchimp api
gem 'gibbon', '~>2.1.2'
## Pagination
gem 'kaminari'
## Default values
gem 'default_value_for', '~> 3.0.0'
## State machine
gem 'state_machines-activerecord', '~> 0.3.0'
## File attachements in amazon s3
gem 'paperclip', '~> 4.3'
gem 'aws-sdk',   '~> 1.6'
## Model decorator
gem 'draper', '~> 1.4.0'
## Search extension
gem 'search_cop'

# required for JS runtime
gem 'therubyracer'

gem 'activeadmin', '~> 1.0.0.pre2'

# Puma web server
gem 'puma'

# Logger
gem 'syslogger', '~> 1.6.0'
gem 'lograge', '~> 0.3.1'

# Scheduler
gem 'whenever', :require => false
# Jobs
gem 'delayed_job_active_record', '~> 4.0.3'
gem "daemons"

# SW Engineering stuff documentation and reference
## bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'yard', group: :doc
gem 'kramdown', groups: [:development, :doc], require: false

group :development do
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  # Development Tools
  gem 'bullet' # Find N + 1 queries
  gem 'rails_best_practices', require: false
  gem 'traceroute', require: false # Fund unused or unreachable routes
  # gem 'rack-mini-profiler', require: false
  # gem 'flamegraph' # graphs for rack-mini-profiler
  gem 'rubocop', require: false
  gem 'rubycritic', require: false
  gem 'brakeman', require: false
  gem 'annotate', require: false # schema in models
  gem 'railroady'
  gem 'flay', require: false
  gem 'flog', require: false
  gem 'mutant-rspec', require: false
  # Automated deployment
  gem 'capistrano', '~> 3.4.0',         require: false
  gem 'capistrano-rvm',                 require: false
  gem 'capistrano-rails', '~> 1.1',     require: false
  gem 'capistrano-bundler', '~> 1.1.2', require: false
  gem 'capistrano3-puma',               require: false
  gem 'capistrano-passenger',           require: false
  gem 'capistrano-ext',                 require: false
  gem 'capistrano3-nginx', '~> 2.0',    require: false
end

group :development, :test do
  gem 'spring'
  gem 'rspec-rails', '3.1.0'
  gem 'libnotify' if /linux/ =~ RUBY_PLATFORM
  gem 'growl' if /darwin/ =~ RUBY_PLATFORM
  gem 'ruby_gntp' if /darwin/ =~ RUBY_PLATFORM
  gem 'guard-rspec', '4.5.0'
  gem 'spork-rails', '4.0.0'
  gem 'guard-spork', '2.1.0'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'faker'
end

group :test do
  gem 'capybara', '2.4.4'
  gem 'cucumber-rails', '1.3.0', require: false
  gem 'database_cleaner'
  gem 'launchy'
  gem 'selenium-webdriver'
  gem 'simplecov', require: false
end

group :production do
  # Supported DBs (only production)
  gem 'mysql2', '~> 0.3.18', platform: :ruby
  gem 'jdbc-mysql', platform: :jruby
  gem 'activerecord-jdbc-adapter', platform: :jruby

  # gem 'rails_12factor', '0.0.2'
end
