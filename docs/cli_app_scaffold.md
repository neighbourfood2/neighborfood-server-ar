## Diseño de la aplicación

### Creación de usuarios

  # from: http://funonrails.com/2011/12/multiple-resources-registrations-with/
  rails g devise User
  rails g model Household --parent User
  rails g model GeneralUser --parent User
  rails g model Admin --parent User
  rails g controller SessionsController

  **Notas**

  Revisar relaciones polimorficas en ves de STI (ya implementado) en: [link](http://blog.jeffsaracco.com/ruby-on-rails-polymorphic-user-model-with-devise-authentication)

### Creación de modelos

  rails generate scaffold Dish name:string description:text price:decimal calories_level:integer fat_level:integer spice_level:integer household_id:integer

  rails generate scaffold Order latitude:decimal longitude:decimal delivery_eta:time comment_food:text comment_order:text state:integer user_id:integer dish_id:integer

  rails generate scaffold DeliveryArea latitude:decimal longitide:decimal radio:integer household_id:integer

  rails generate scaffold MenuDish date:date dish_id:integer household_id:integer quantity:integer total_left:integer

  rails generate model Avaliability available_time_start:time  available_time_end:time