<style type="text/css">
.diagram {
  display: block;
  width: 100%;
  height: 100%;
}
</style>

# Architecture and UML design

## Structure

![Neighbourfood Structure Diagram](uml/UML_NF_relation.png)

## Classes

![Neighbourfood Class Diagram](uml/UML_NF_classes.png)

## Order LC

![Neighbourfood Order Life Cycle](uml/UML_NF_order_LC.png)

## Current UML

generate it with...

    rake diagram:all

### Models

<img src="uml/models_brief.svg" class="diagram">
<img src="uml/models_complete.svg" class="diagram">

[models](uml/models_complete.svg)

[models brief](uml/models_brief.svg)

### Controllers

<img src="uml/controllers_complete.svg" class="diagram">

[controllers](uml/controllers_complete.svg)

[controllers brief](uml/controllers_brief.svg)

