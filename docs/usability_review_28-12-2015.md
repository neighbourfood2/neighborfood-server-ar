# Usability review

* Revisar todas las traducciones de mensajes para devise y modelos

## Ordenes

* Actualizar formulario de pago para mostrar:
  - link a paypal (usuario y contraseña de paypal) [SO] Listo manejado por paypal.
  - Mejorar el formulario de tarjeta de credito [SO] Por ahora solo usaremos 
    paypal ya que necesitamos procesar los pagos en pesos y no en USDs.
  - Poner explicacion de CVV2 (tooltip) [SO] Por ahora no.
  - Almacenar informacion de pago tipo Uber [SO] Por ahora no.
  - El tiempo de confiramcion de pago tarda mucho

* En la confiramcion de orden:
  - Cantidad de lo que pagaste [SO] Listo "total"
  - Imagen y nombre de lo que compraste [SO] Listo, solo nombre
  - Quitar link a editar orden [SO] Listo
  - Poner un text box para comentario [SO] Listo
  - Agregar opciones de entrega
  - Añadir opcion de cantidad de pedidos
  - Añadir carrito de compra (baja prioridd)
  - Poner el detalle de la entrega
  - Añadir logica para el campo de "address" en las ordenes

* En la orden de ama de casa:
  - Que se muestre la informacion del usuario cliente
  - Que se muestre su telefono

## Menus

* Poner en el indice principal de menus titulo de "Menus del dia" [SO] Listo

* En el indice de menus:
  - Que la ubicacion tenga un tooltip con la direccion o un link

* En el show de menus:
  - Descripcion del platillo principal
  - Descripcion, bebidas, acompañantes, etc
  - Imagen del platillo(s)

## Perfil Usuario

* Poder dar de alta la zona del usuario

## Perfil Husehold

* Poner los platillos
* Menus para el dia de hoy
* Zona de entrega
* Rating

## Promos

* Implementar mecanismo de pomociones tipo uber

## Otro

* Revisar ortografia
* cambiar menus a comidas

