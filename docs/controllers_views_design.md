# Web app controllers and views design

## portal de la ama de casa (household)

(In the households namespace)
resources households/
resources households/:household_id/dishes
resources households/:household_id/today_menus/:menu_id
resources households/:household_id/orders
resources households/:household_id/delivery_areas


#### Mis ordenes
En esta sección el ama de casa puede ver el historial de ordenes y filtrar las órdenes por su status, fecha, o las las del dia de hoy. (Se filtra por parametros en la ruta)

#### Mis platillos
En esta sección el ama de casa puede ver la colección de platillos que ha cocinado o sabe hacer, la idea es crear un catálogo de lo que ha echo o sabe hacer para que después sea más fácil seleccionarlos a la hora de crear el menu del dia. (Se filtra por parametros en la ruta)

#### Menu del dia
En esta sección se crean los menús diarios. A partir de los platillos definidos en la sección de mis platillos se escoge un número de ellos para ser elaborados el día de hoy. Estos platillos son los que se muestran al público para ver qué pedir.

#### Area de entrega
En esta sección se define el área o áreas en donde la ama de casa puede entregar su comida. 

## portal del usuario general

resources dishes/
resources general_users/:user_id/orders
resources general_users/:user_id/payments

#### comida cerca
Muestra la lista de comida por cercanía con datos del ama de casa.

### Mis ordenes
Muestra mi historial de ordenes y status
