# ## Schema Information
#
# Table name: `menus`
#
# ### Columns
#
# Name                    | Type               | Attributes
# ----------------------- | ------------------ | ---------------------------
# **`id`**                | `integer`          | `not null, primary key`
# **`created_at`**        | `datetime`         | `not null`
# **`updated_at`**        | `datetime`         | `not null`
# **`date`**              | `date`             |
# **`household_id`**      | `integer`          |
# **`raw_price`**         | `decimal(16, 2)`   |
# **`quantity`**          | `integer`          |
# **`total_left`**        | `integer`          |
# **`delivery_eta`**      | `datetime`         |
# **`description`**       | `text`             |
# **`is_archived`**       | `boolean`          |
# **`charge`**            | `decimal(16, 2)`   |
# **`delivery_area_id`**  | `integer`          |
# **`block_sale`**        | `boolean`          |
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu do
    delivery_eta Faker::Time.between(DateTime.now, DateTime.now + 12.hours)
    raw_price Faker::Number.decimal(2)
    quantity Faker::Number.number(2)
    household
  end
end
