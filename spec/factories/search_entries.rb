# ## Schema Information
#
# Table name: `search_entries`
#
# ### Columns
#
# Name               | Type               | Attributes
# ------------------ | ------------------ | ---------------------------
# **`id`**           | `integer`          | `not null, primary key`
# **`text_entry`**   | `text`             |
# **`field1`**       | `string`           |
# **`field2`**       | `string`           |
# **`field3`**       | `string`           |
# **`search_type`**  | `string`           |
# **`created_at`**   | `datetime`         | `not null`
# **`updated_at`**   | `datetime`         | `not null`
#

FactoryGirl.define do
  factory :search_entry do
  end
end
