# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :household, parent: :user, class: 'Household' do
    tmp_location
  end
end
