# ## Schema Information
#
# Table name: `menu_dishes`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`menu_id`**       | `integer`          |
# **`dish_id`**       | `integer`          |
# **`quantity`**      | `integer`          |
# **`total_left`**    | `integer`          |
# **`is_main_dish`**  | `boolean`          |
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :menu_dish do
  end
end
