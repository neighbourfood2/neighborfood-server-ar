FactoryGirl.define do
  factory :payment_paypal, parent: :payment_basis, class: 'Payment::Paypal' do
    type 'paypal'
  end
end
