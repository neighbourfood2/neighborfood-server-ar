FactoryGirl.define do
  factory :payment_credit_card, parent: :payment_basis, class: 'Payment::CreditCard' do
    type 'credit_card'
  end
end
