FactoryGirl.define do
  factory :payment_basis, class: 'Payment::Base' do
    type 'base'
    sale Faker::Number.number(10)
    description Faker::Lorem.sentence
    payed_at Faker::Date.forward(23)
    charge Faker::Number.decimal(2)
  end
end
