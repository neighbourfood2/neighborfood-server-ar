# ## Schema Information
#
# Table name: `delivery_areas`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`latitude`**      | `decimal(10, 6)`   |
# **`radius`**        | `integer`          |
# **`household_id`**  | `integer`          |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`longitude`**     | `decimal(10, 6)`   |
# **`address`**       | `text`             |
# **`notes`**         | `text`             |
# **`is_archived`**   | `boolean`          |
# **`name`**          | `string`           |
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :delivery_area do
    latitude '9.99'
    longitude '9.99'
    radio 1
    household_id 1
  end
end
