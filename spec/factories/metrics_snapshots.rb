# ## Schema Information
#
# Table name: `metrics_snapshots`
#
# ### Columns
#
# Name                  | Type               | Attributes
# --------------------- | ------------------ | ---------------------------
# **`id`**              | `integer`          | `not null, primary key`
# **`total_sale`**      | `integer`          |
# **`total_charges`**   | `integer`          |
# **`sign_ups_count`**  | `integer`          |
# **`sign_ins_count`**  | `integer`          |
# **`total_users`**     | `integer`          |
# **`date`**            | `datetime`         |
# **`created_at`**      | `datetime`         | `not null`
# **`updated_at`**      | `datetime`         | `not null`
#

FactoryGirl.define do
  factory :metrics_snapshot do
    total_sale 1
total_charges 1
sign_ups_count 1
sign_ins_count 1
total_users 1
date "2016-05-27 18:23:08"
  end

end
