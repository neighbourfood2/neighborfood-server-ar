# ## Schema Information
#
# Table name: `orders`
#
# ### Columns
#
# Name                   | Type               | Attributes
# ---------------------- | ------------------ | ---------------------------
# **`id`**               | `integer`          | `not null, primary key`
# **`latitude`**         | `decimal(10, 6)`   |
# **`longitude`**        | `decimal(10, 6)`   |
# **`delivery_eta`**     | `datetime`         |
# **`comment_food`**     | `text`             |
# **`comment_order`**    | `text`             |
# **`user_id`**          | `integer`          |
# **`menu_id`**          | `integer`          |
# **`created_at`**       | `datetime`         | `not null`
# **`updated_at`**       | `datetime`         | `not null`
# **`household_id`**     | `integer`          |
# **`general_user_id`**  | `integer`          |
# **`state`**            | `string`           |
# **`comment_rate`**     | `text`             |
# **`quantity`**         | `integer`          |
# **`address`**          | `string`           |
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :order do
  end
end
