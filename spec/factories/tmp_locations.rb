# ## Schema Information
#
# Table name: `tmp_locations`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `integer`          | `not null, primary key`
# **`name`**        | `string`           |
# **`address`**     | `string`           |
# **`latitude`**    | `decimal(10, 6)`   |
# **`longitude`**   | `decimal(10, 6)`   |
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :tmp_location do
    name 'MyString'
    address 'MyString'
    latitude '9.99'
    longitude '9.99'
  end
end
