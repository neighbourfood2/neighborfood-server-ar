# ## Schema Information
#
# Table name: `dishes`
#
# ### Columns
#
# Name                      | Type               | Attributes
# ------------------------- | ------------------ | ---------------------------
# **`id`**                  | `integer`          | `not null, primary key`
# **`name`**                | `string`           |
# **`description`**         | `text`             |
# **`price`**               | `decimal(16, 2)`   |
# **`calories_level`**      | `integer`          |
# **`fat_level`**           | `integer`          |
# **`spice_level`**         | `integer`          |
# **`household_id`**        | `integer`          |
# **`created_at`**          | `datetime`         | `not null`
# **`updated_at`**          | `datetime`         | `not null`
# **`image_file_name`**     | `string`           |
# **`image_content_type`**  | `string`           |
# **`image_file_size`**     | `integer`          |
# **`image_updated_at`**    | `datetime`         |
# **`is_archived`**         | `boolean`          | `default(FALSE), not null`
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dish do
    name Faker::Lorem.word
    description Faker::Lorem.sentence
    price Faker::Number.decimal(2)
    household
    image Faker::Avatar.image
  end
end
