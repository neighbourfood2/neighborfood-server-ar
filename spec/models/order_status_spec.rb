# ## Schema Information
#
# Table name: `order_statuses`
#
# ### Columns
#
# Name                     | Type               | Attributes
# ------------------------ | ------------------ | ---------------------------
# **`id`**                 | `integer`          | `not null, primary key`
# **`name`**               | `string`           |
# **`created_at`**         | `datetime`         | `not null`
# **`updated_at`**         | `datetime`         | `not null`
# **`status_identifier`**  | `string`           |
#

require 'rails_helper'

RSpec.describe OrderStatus, type: :model do
  let(:order_stat) { FactoryGirl.create(:order_status) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:order_status)).to be_kind_of OrderStatus
  end

  describe 'model attributes' do
    # Attributes
    it { expect(order_stat).to respond_to 'id' }
    it { expect(order_stat).to respond_to 'name' }
    it { expect(order_stat).to respond_to 'created_at' }
    it { expect(order_stat).to respond_to 'updated_at' }
    it { expect(order_stat).to respond_to 'status_identifier' }
    # Relationships
  end

  describe 'model methods' do
  end
end
