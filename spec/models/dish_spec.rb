# ## Schema Information
#
# Table name: `dishes`
#
# ### Columns
#
# Name                      | Type               | Attributes
# ------------------------- | ------------------ | ---------------------------
# **`id`**                  | `integer`          | `not null, primary key`
# **`name`**                | `string`           |
# **`description`**         | `text`             |
# **`price`**               | `decimal(16, 2)`   |
# **`calories_level`**      | `integer`          |
# **`fat_level`**           | `integer`          |
# **`spice_level`**         | `integer`          |
# **`household_id`**        | `integer`          |
# **`created_at`**          | `datetime`         | `not null`
# **`updated_at`**          | `datetime`         | `not null`
# **`image_file_name`**     | `string`           |
# **`image_content_type`**  | `string`           |
# **`image_file_size`**     | `integer`          |
# **`image_updated_at`**    | `datetime`         |
# **`is_archived`**         | `boolean`          | `default(FALSE), not null`
#

require 'rails_helper'

RSpec.describe Dish, type: :model do
  let(:dish) { FactoryGirl.create(:dish) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:dish)).to be_kind_of Dish
  end

  describe 'model attributes' do
    # Attributes
    it { expect(dish).to respond_to 'id' }
    it { expect(dish).to respond_to 'name' }
    it { expect(dish).to respond_to 'description' }
    it { expect(dish).to respond_to 'price' }
    it { expect(dish).to respond_to 'calories_level' }
    it { expect(dish).to respond_to 'fat_level' }
    it { expect(dish).to respond_to 'spice_level' }
    # Relationships
    it { expect(dish).to respond_to 'menus' }
    it { expect(dish).to respond_to 'menu_dishes' }
    it { expect(dish).to respond_to 'household' }
    it { expect(dish).not_to respond_to 'orders' }
    # Attachments
    it { expect(dish).to respond_to 'image' }
  end

  describe 'model methods' do
  end
end
