# ## Schema Information
#
# Table name: `delivery_areas`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`latitude`**      | `decimal(10, 6)`   |
# **`radio`**         | `integer`          |
# **`household_id`**  | `integer`          |
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`longitude`**     | `decimal(10, 6)`   |
#

require 'rails_helper'

RSpec.describe DeliveryArea, type: :model do
  let(:delivery_area) { FactoryGirl.create(:delivery_area) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:delivery_area)).to be_kind_of DeliveryArea
  end

  describe 'model attributes' do
    # Attributes
    it { expect(delivery_area).to respond_to 'id' }
    it { expect(delivery_area).to respond_to 'latitude' }
    it { expect(delivery_area).to respond_to 'longitude' }
    it { expect(delivery_area).to respond_to 'radio' }
    # Relationships
    it { expect(delivery_area).to respond_to 'household' }
  end

  describe 'model methods' do
  end
end
