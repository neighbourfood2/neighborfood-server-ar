# ## Schema Information
#
# Table name: `payments`
#
# ### Columns
#
# Name                        | Type               | Attributes
# --------------------------- | ------------------ | ---------------------------
# **`id`**                    | `integer`          | `not null, primary key`
# **`type`**                  | `string`           |
# **`sale`**                  | `decimal(10, 2)`   |
# **`description`**           | `string`           |
# **`paypal_redirect_urls`**  | `text`             |
# **`payed_at`**              | `datetime`         |
# **`transaction_id`**        | `string`           |
# **`payable_id`**            | `integer`          |
# **`payable_type`**          | `string`           |
# **`created_at`**            | `datetime`         | `not null`
# **`updated_at`**            | `datetime`         | `not null`
# **`charge`**                | `decimal(16, 2)`   |
#

require 'rails_helper'

RSpec.describe Payment::Base, type: :model do
  let(:payment) { FactoryGirl.create(:payment_basis) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:payment_basis)).to be_kind_of Payment::Base
  end

  describe 'model attributes' do
    # Attributes
    it { expect(payment).to respond_to 'id' }
    it { expect(payment).to respond_to 'type' }
    it { expect(payment).to respond_to 'sale' }
    it { expect(payment).to respond_to 'description' }
    it { expect(payment).to respond_to 'paypal_redirect_urls' }
    it { expect(payment).to respond_to 'payed_at' }
    it { expect(payment).to respond_to 'transaction_id' }
    it { expect(payment).to respond_to 'payable_id' }
    it { expect(payment).to respond_to 'payable_type' }
    it { expect(payment).to respond_to 'created_at' }
    it { expect(payment).to respond_to 'updated_at' }
    it { expect(payment).to respond_to 'charge' }
    # Relationships
  end

  describe 'model methods' do
  end
end
