# ## Schema Information
#
# Table name: `orders`
#
# ### Columns
#
# Name                   | Type               | Attributes
# ---------------------- | ------------------ | ---------------------------
# **`id`**               | `integer`          | `not null, primary key`
# **`latitude`**         | `decimal(10, 6)`   |
# **`longitude`**        | `decimal(10, 6)`   |
# **`delivery_eta`**     | `datetime`         |
# **`comment_food`**     | `text`             |
# **`comment_order`**    | `text`             |
# **`user_id`**          | `integer`          |
# **`menu_id`**          | `integer`          |
# **`created_at`**       | `datetime`         | `not null`
# **`updated_at`**       | `datetime`         | `not null`
# **`household_id`**     | `integer`          |
# **`general_user_id`**  | `integer`          |
# **`state`**            | `string`           |
# **`comment_rate`**     | `text`             |
# **`quantity`**         | `integer`          |
# **`address`**          | `string`           |
#

require 'rails_helper'

RSpec.describe Order, type: :model do
  let(:order) { FactoryGirl.build(:order) }

  it 'has an asociated factory' do
    expect(FactoryGirl.build(:order)).to be_kind_of Order
  end

  describe 'model attributes' do
    # Attributes
    it { expect(order).to respond_to 'id' }
    it { expect(order).to respond_to 'latitude' }
    it { expect(order).to respond_to 'longitude' }
    it { expect(order).to respond_to 'delivery_eta' }
    it { expect(order).to respond_to 'comment_food' }
    it { expect(order).to respond_to 'comment_order' }
    it { expect(order).to respond_to 'state' }
    # Relationships
    it { expect(order).to respond_to 'general_user' }
    it { expect(order).to respond_to 'menu' }
    it { expect(order).not_to respond_to 'dish' }
  end

  describe 'model methods' do
  end
end
