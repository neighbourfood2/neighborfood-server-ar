# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`id`**                      | `integer`          | `not null, primary key`
# **`email`**                   | `string`           | `default(""), not null`
# **`encrypted_password`**      | `string`           | `default(""), not null`
# **`reset_password_token`**    | `string`           |
# **`reset_password_sent_at`**  | `datetime`         |
# **`remember_created_at`**     | `datetime`         |
# **`sign_in_count`**           | `integer`          | `default(0), not null`
# **`current_sign_in_at`**      | `datetime`         |
# **`last_sign_in_at`**         | `datetime`         |
# **`current_sign_in_ip`**      | `string`           |
# **`last_sign_in_ip`**         | `string`           |
# **`created_at`**              | `datetime`         | `not null`
# **`updated_at`**              | `datetime`         | `not null`
# **`type`**                    | `string`           |
# **`avatar_file_name`**        | `string`           |
# **`avatar_content_type`**     | `string`           |
# **`avatar_file_size`**        | `integer`          |
# **`avatar_updated_at`**       | `datetime`         |
# **`first_name`**              | `string`           |
# **`last_name`**               | `string`           |
# **`phone`**                   | `string`           |
# **`latitude`**                | `decimal(10, 6)`   |
# **`longitude`**               | `decimal(10, 6)`   |
# **`tmp_location_id`**         | `integer`          |
#

require 'rails_helper'

RSpec.describe Household, type: :model do
  let(:h_hold) { FactoryGirl.create(:household) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:household)).to be_kind_of Household
  end

  describe 'model attributes' do
    # Attributes
    it { expect(h_hold).to respond_to 'id' }
    it { expect(h_hold).to respond_to 'email' }
    it { expect(h_hold).to respond_to 'password' }
    it { expect(h_hold).to respond_to 'first_name' }
    it { expect(h_hold).to respond_to 'last_name' }
    it { expect(h_hold).to respond_to 'phone' }
    it { expect(h_hold).to respond_to 'type' }
    it { expect(h_hold).to respond_to 'latitude' }
    it { expect(h_hold).to respond_to 'longitude' }
    # Relationships
    it { expect(h_hold).to respond_to 'dishes' }
    it { expect(h_hold).to respond_to 'menus' }
    it { expect(h_hold).to respond_to 'delivery_areas' }
    it { expect(h_hold).to respond_to 'tmp_location' }
    # Attachements
    it { expect(h_hold).to respond_to 'avatar' }
  end

  describe 'model methods' do
  end
end
