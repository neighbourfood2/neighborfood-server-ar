# ## Schema Information
#
# Table name: `menu_dishes`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`menu_id`**       | `integer`          |
# **`dish_id`**       | `integer`          |
# **`quantity`**      | `integer`          |
# **`total_left`**    | `integer`          |
# **`is_main_dish`**  | `boolean`          |
#

require 'rails_helper'

RSpec.describe MenuDish, type: :model do
  let(:menu_dish) { FactoryGirl.create(:menu_dish) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:menu_dish)).to be_kind_of MenuDish
  end

  describe 'model attributes' do
    # Attributes
    it { expect(menu_dish).to respond_to 'id' }
    it { expect(menu_dish).to respond_to 'quantity' }
    it { expect(menu_dish).to respond_to 'total_left' }
    # Relationships
    it { expect(menu_dish).to respond_to 'menu' }
    it { expect(menu_dish).to respond_to 'dish' }
  end

  describe 'model methods' do
  end
end
