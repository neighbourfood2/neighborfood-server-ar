# ## Schema Information
#
# Table name: `menus`
#
# ### Columns
#
# Name                | Type               | Attributes
# ------------------- | ------------------ | ---------------------------
# **`id`**            | `integer`          | `not null, primary key`
# **`created_at`**    | `datetime`         | `not null`
# **`updated_at`**    | `datetime`         | `not null`
# **`date`**          | `date`             |
# **`household_id`**  | `integer`          |
# **`raw_price`**     | `decimal(16, 2)`   |
# **`quantity`**      | `integer`          |
# **`total_left`**    | `integer`          |
# **`delivery_eta`**  | `datetime`         |
# **`description`**   | `text`             |
# **`is_archived`**   | `boolean`          |
# **`charge`**        | `decimal(16, 2)`   |
#

require 'rails_helper'

RSpec.describe Menu, type: :model do
  let(:menu) { FactoryGirl.build(:menu) }

  it 'has an asociated factory' do
    expect(FactoryGirl.build(:menu)).to be_kind_of Menu
  end

  describe 'model attributes' do
    # Attributes
    it { expect(menu).to respond_to 'id' }
    it { expect(menu).to respond_to 'date' }
    # Relationships
    it { expect(menu).to respond_to 'household' }
    it { expect(menu).to respond_to 'dishes' }
    it { expect(menu).to respond_to 'menu_dishes' }
    it { expect(menu).to respond_to 'orders' }
  end

  describe 'model methods' do
  end
end
