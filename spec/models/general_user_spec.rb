# ## Schema Information
#
# Table name: `users`
#
# ### Columns
#
# Name                          | Type               | Attributes
# ----------------------------- | ------------------ | ---------------------------
# **`id`**                      | `integer`          | `not null, primary key`
# **`email`**                   | `string`           | `default(""), not null`
# **`encrypted_password`**      | `string`           | `default(""), not null`
# **`reset_password_token`**    | `string`           |
# **`reset_password_sent_at`**  | `datetime`         |
# **`remember_created_at`**     | `datetime`         |
# **`sign_in_count`**           | `integer`          | `default(0), not null`
# **`current_sign_in_at`**      | `datetime`         |
# **`last_sign_in_at`**         | `datetime`         |
# **`current_sign_in_ip`**      | `string`           |
# **`last_sign_in_ip`**         | `string`           |
# **`created_at`**              | `datetime`         | `not null`
# **`updated_at`**              | `datetime`         | `not null`
# **`type`**                    | `string`           |
# **`avatar_file_name`**        | `string`           |
# **`avatar_content_type`**     | `string`           |
# **`avatar_file_size`**        | `integer`          |
# **`avatar_updated_at`**       | `datetime`         |
# **`first_name`**              | `string`           |
# **`last_name`**               | `string`           |
# **`phone`**                   | `string`           |
# **`latitude`**                | `decimal(10, 6)`   |
# **`longitude`**               | `decimal(10, 6)`   |
# **`tmp_location_id`**         | `integer`          |
#

require 'rails_helper'

RSpec.describe GeneralUser, type: :model do
  let(:g_user) { FactoryGirl.create(:general_user) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:general_user)).to be_kind_of GeneralUser
  end

  describe 'model attributes' do
    # Attributes
    it { expect(g_user).to respond_to 'id' }
    it { expect(g_user).to respond_to 'email' }
    it { expect(g_user).to respond_to 'password' }
    it { expect(g_user).to respond_to 'first_name' }
    it { expect(g_user).to respond_to 'last_name' }
    it { expect(g_user).to respond_to 'phone' }
    it { expect(g_user).to respond_to 'type' }
    it { expect(g_user).to respond_to 'latitude' }
    it { expect(g_user).to respond_to 'longitude' }
    # Relationships
    it { expect(g_user).to respond_to 'orders' }
    # Attachments
    it { expect(g_user).to respond_to 'avatar' }
  end

  describe 'model methods' do
  end
end
