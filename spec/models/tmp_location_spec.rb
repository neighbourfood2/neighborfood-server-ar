# ## Schema Information
#
# Table name: `tmp_locations`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `integer`          | `not null, primary key`
# **`name`**        | `string`           |
# **`address`**     | `string`           |
# **`latitude`**    | `decimal(10, 6)`   |
# **`longitude`**   | `decimal(10, 6)`   |
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

require 'rails_helper'

RSpec.describe TmpLocation, type: :model do
  let(:t_location) { FactoryGirl.create(:tmp_location) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:tmp_location)).to be_kind_of TmpLocation
  end

  describe 'model attributes' do
    # Attributes
    it { expect(t_location).to respond_to 'id' }
    it { expect(t_location).to respond_to 'name' }
    it { expect(t_location).to respond_to 'address' }
    it { expect(t_location).to respond_to 'latitude' }
    it { expect(t_location).to respond_to 'longitude' }
    # Relationships
    it { expect(t_location).to respond_to 'households' }
    # Attachements
  end

  describe 'model methods' do
  end
end
