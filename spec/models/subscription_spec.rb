# ## Schema Information
#
# Table name: `subscriptions`
#
# ### Columns
#
# Name              | Type               | Attributes
# ----------------- | ------------------ | ---------------------------
# **`id`**          | `integer`          | `not null, primary key`
# **`created_at`**  | `datetime`         | `not null`
# **`updated_at`**  | `datetime`         | `not null`
#

require 'rails_helper'

RSpec.describe Subscription, type: :model do
  let(:subscription) { FactoryGirl.create(:subscription) }

  it 'has an asociated factory' do
    expect(FactoryGirl.create(:subscription)).to be_kind_of Subscription
  end

  describe 'model attributes' do
  end

  describe 'model methods' do
  end
end
