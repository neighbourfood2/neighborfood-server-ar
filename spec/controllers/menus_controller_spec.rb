# ## Schema Information
#
# Table name: `menus`
#
# ### Columns
#
# Name                    | Type               | Attributes
# ----------------------- | ------------------ | ---------------------------
# **`id`**                | `integer`          | `not null, primary key`
# **`created_at`**        | `datetime`         | `not null`
# **`updated_at`**        | `datetime`         | `not null`
# **`date`**              | `date`             |
# **`household_id`**      | `integer`          |
# **`raw_price`**         | `decimal(16, 2)`   |
# **`quantity`**          | `integer`          |
# **`total_left`**        | `integer`          |
# **`delivery_eta`**      | `datetime`         |
# **`description`**       | `text`             |
# **`is_archived`**       | `boolean`          |
# **`charge`**            | `decimal(16, 2)`   |
# **`delivery_area_id`**  | `integer`          |
# **`block_sale`**        | `boolean`          |
#

require 'rails_helper'

RSpec.describe MenusController, type: :controller do
  let(:valid_attributes) do
    skip('Add a hash of attributes valid for your model')
  end

  let(:invalid_attributes) do
    skip('Add a hash of attributes invalid for your model')
  end

  let(:valid_session) { {} }

  describe 'GET index' do
    it 'assigns all menus as @menus' do
      menu = Menu.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:menus)).to eq([menu])
    end
  end
end
