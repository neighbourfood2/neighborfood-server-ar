# in config/initializers/locale.rb

# set default locale to something other than :en
I18n.enforce_available_locales = false
I18n.available_locales = [:en, :es]
I18n.default_locale = :es
