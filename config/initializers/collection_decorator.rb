class Draper::CollectionDecorator
  delegate :current_page, :total_pages, :limit_value
end
Draper::CollectionDecorator.send :delegate, :per_page_kaminari
