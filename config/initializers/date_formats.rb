Date::DATE_FORMATS[:short]    = proc { |date| date.stamp('Sun Jan 5') }
Time::DATE_FORMATS[:military] = proc { |time| time.stamp('5 Jan 23:59') }
