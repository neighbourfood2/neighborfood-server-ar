Rails.application.routes.draw do
  
  constraints subdomain: false do
    get ':any', to: redirect(subdomain: 'www', path: '/%{any}'), any: /.*/
  end

  root to: 'landing_page#index'
  # sitemap
  get 'sitemap.xml', to: 'sitemaps#index'
  #
  post 'subscribe', to: 'landing_page#subscribe'
  post 'contact', to: 'landing_page#contact'
  post 'subscribe', to: 'landing_page#subscribe'
  post 'suscribe_location', to: 'landing_page#suscribe_location'
  resources :menus, only: [:index, :show]
  post '/rate' => 'rater#create', :as => 'rate'
  # Static Pages
  get 'contact', to: 'static_pages#contact'
  get 'contact/cookers', to: 'static_pages#contact_cookers'
  get 'contact/service', to: 'static_pages#contact_service'
  get 'contact/general', to: 'static_pages#contact_general'
  get 'about_us', to: 'static_pages#about_us'
  get 'legal', to: 'static_pages#legal'
  get 'company', to: 'static_pages#company'
  get 'help', to: 'static_pages#help'
  get 'block_sale', to: 'static_pages#block_sale'

  # Devise Users
  devise_for :users, controllers: { passwords: 'users/passwords' }, skip: [:registrations, :sessions]
  devise_scope :user do
    delete '/logout', to: 'users/sessions#destroy', as: :destroy_user_session
    get '/login', to: 'users/sessions#new', as: :new_user_session
    post '/login', to: 'users/sessions#create', as: :user_session
  end
  devise_for :households, controllers: { registrations: 'households/registrations' }, skip: :sessions
  devise_for :general_users, controllers: { registrations: 'general_users/registrations' }, skip: :sessions

  # Households
  resources :households, only: [:show] do
    resources :delivery_areas, controller: 'households/delivery_areas'
    resources :orders, controller: 'households/orders', skip: [:new, :edit]
    post '/orders/:order_id/deliver', to: 'households/orders#deliver', as: :order_deliver
    resources :dishes, controller: 'households/dishes'
    resources :menus, controller: 'households/menus'
  end

  # General Users
  resources :general_users, only: [:show] do
    resources :orders, controller: 'general_users/orders'
  end

  # Payments
  namespace :payments do
    resource :credit_card, controller: 'credit_card', only: [:new, :create]
    resource :paypal, controller: 'paypal', only: [:create]
    get 'paypal/paypalconfirm', to: 'paypal#paypal_confirm', as: :paypal_confirm
    post 'paypal/paypalconfirmwebhook', to: 'paypal#paypal_confirm_webhook', as: :paypal_confirm_webhook
  end

  # Admin
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
end
