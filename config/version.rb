module NeighbourFood
  def self.version
    VERSION::STRING
  end

  module VERSION
    MAJOR = 0
    MINOR = 5
    TINY  = 0
    PRE   = ''

    STRING = [MAJOR, MINOR, TINY, PRE].compact.join('.').gsub(/.$/, '')
  end
end
