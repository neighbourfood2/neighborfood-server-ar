# HEAD

# Cambios del review del 23 de mayo del 2016

1. Poner numero de whatsapp en la barra de navegación

2. 
http://www.neighbourfood.mx/
agregar "para poder llegar a ti" en la pregutna de la direccion
En el formulario de tu direccion, agregar al text field:
"e.g. Av. Patria 429-59, Zapopan, Jalisco"

3.
http://www.neighbourfood.mx/contact/cookers
Agregar validadores a los campos para que no se envie vacio

4.
http://www.neighbourfood.mx/menus
https://github.com/svenfuchs/rails-i18n/blob/master/rails/locale/es.yml
Traducir las fechas en los menus

5.
http://www.neighbourfood.mx/menus/130
En la orden poner un texto que diga que si quieres pagar en efectivo usar inbox o whatsapp.

6.
http://www.neighbourfood.mx/menus/130
poner validadores en la direccion de la orden

7. el color de la hora de entrega se puso naranja



# v0.2.3 to v0.4.3 - April 03, 2026

- Corrected last dot in version string
- Bootstrap front end framework added
- Removed bourbon styles
- Mensaje de aplicacion alfa
- Fixes from review march 3

# v0.2.0.alpha to v0.2.3.alpha - January 10, 2016

- Fixed error about missing image in dish
- Works in automated deployment with capistrano
- Works in documentation
- Enhanced payments classes structure
- Fixed some usability issues from the review
- Second release, still in development

# v0.1.0.alpha - December 29, 2015

- First version for neighbourfood app